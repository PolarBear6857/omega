﻿using System.Windows;
using Omega.DatabaseClasses.ClenoveSboru;
using Omega.DatabaseClasses.ClenoveSmeny;
using Omega.DatabaseClasses.Smeny;
using Omega.DatabaseClasses.VozidlaTechnika;
using Omega.DatabaseClasses.Vyjezd;
using Omega.DatabaseClasses.VyjezdUdalosti;

namespace Omega.OperationHandlers
{
    /// <summary>
    /// Class for updating database tables.
    /// </summary>
    public class UpdateHandler
    {
        /// <summary>
        /// Initializes a new instance of the Update class.
        /// </summary>
        public UpdateHandler()
        {
            if (MainWindow.ConfigurationFile)
            {
                string tableName = MainWindow.PromptForTableName();

                if (!string.IsNullOrEmpty(tableName))
                {
                    switch (tableName.ToLower())
                    {
                        case "clenovesmeny":
                            MessageBox.Show($"Table Name: {tableName}", "Table Selected");
                            ClenoveSmenyUpdate clenoveSmeny = new ClenoveSmenyUpdate();
                            clenoveSmeny.Show();
                            break;
                        case "smeny":
                            MessageBox.Show($"Table Name: {tableName}", "Table Selected");
                            SmenyUpdate smeny = new SmenyUpdate();
                            smeny.Show();
                            break;
                        case "clenovesboru":
                            MessageBox.Show($"Table Name: {tableName}", "Table Selected");
                            ClenoveSboruUpdate clenoveSboru = new ClenoveSboruUpdate();
                            clenoveSboru.Show();
                            break;
                        case "vyjezd":
                            MessageBox.Show($"Table Name: {tableName}", "Table Selected");
                            VyjezdUpdate vyjezd = new VyjezdUpdate();
                            vyjezd.Show();
                            break;
                        case "vyjezdudalosti":
                            MessageBox.Show($"Table Name: {tableName}", "Table Selected");
                            VyjezdUdalostiUpdate vyjezdUdalosti = new VyjezdUdalostiUpdate();
                            vyjezdUdalosti.Show();
                            break;
                        case "vozidlatechnika":
                            MessageBox.Show($"Table Name: {tableName}", "Table Selected");
                            VozidlaTechnikaUpdate vozidlaTechnika = new VozidlaTechnikaUpdate();
                            vozidlaTechnika.Show();
                            break;
                        case "":
                            MessageBox.Show("No valid table has been selected", "Error");
                            break;
                        default:
                            MessageBox.Show("No valid table has been selected", "Error");
                            break;
                    }
                }
            }
            else
            {
                MessageBox.Show("No configuration file selected", "Error");
            }
        }
    }
}