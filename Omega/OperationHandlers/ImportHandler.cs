﻿using System;
using System.Windows;
using Omega.DatabaseClasses.ClenoveSboru;
using Omega.DatabaseClasses.ClenoveSmeny;
using Omega.DatabaseClasses.Smeny;
using Omega.DatabaseClasses.VozidlaTechnika;
using Omega.DatabaseClasses.Vyjezd;
using Omega.DatabaseClasses.VyjezdUdalosti;

namespace Omega.OperationHandlers
{
    /// <summary>
    /// Class for importing data into database tables.
    /// </summary>
    public class ImportHandler
    {
        /// <summary>
        /// Initializes a new instance of the Import class.
        /// </summary>
        /// <exception cref="InvalidOperationException">Thrown when an invalid operation is performed.</exception>
        public ImportHandler()
        {
            if (MainWindow.ConfigurationFile)
            {
                string tableName = MainWindow.PromptForTableName();

                if (!string.IsNullOrEmpty(tableName))
                {
                    switch (tableName.ToLower())
                    {
                        case "clenovesmeny":
                            MessageBox.Show($"Table Name: {tableName}", "Table Selected");
                            MessageBox.Show("The CSV file must not include ID columns", "Warning");
                            DatabaseClenoveSmenyGateway databaseClenoveSmeny = new DatabaseClenoveSmenyGateway();
                            databaseClenoveSmeny.Import(new MainWindow().PromptForImportCsv() ??
                                                        throw new InvalidOperationException());
                            break;
                        case "smeny":
                            MessageBox.Show($"Table Name: {tableName}", "Table Selected");
                            MessageBox.Show("The CSV file must not include ID columns", "Warning");
                            DatabaseSmenyGateway databaseSmeny = new DatabaseSmenyGateway();
                            databaseSmeny.Import(new MainWindow().PromptForImportCsv() ??
                                                 throw new InvalidOperationException());
                            break;
                        case "clenovesboru":
                            MessageBox.Show($"Table Name: {tableName}", "Table Selected");
                            MessageBox.Show("The CSV file must not include ID columns", "Warning");
                            DatabaseClenoveSboruGateway databaseClenoveSboru = new DatabaseClenoveSboruGateway();
                            databaseClenoveSboru.Import(new MainWindow().PromptForImportCsv() ??
                                                        throw new InvalidOperationException());
                            break;
                        case "vyjezd":
                            MessageBox.Show($"Table Name: {tableName}", "Table Selected");
                            MessageBox.Show("The CSV file must not include ID columns", "Warning");
                            DatabaseVyjezdGateway databaseVyjezd = new DatabaseVyjezdGateway();
                            databaseVyjezd.Import(new MainWindow().PromptForImportCsv() ??
                                                  throw new InvalidOperationException());
                            break;
                        case "vyjezdudalosti":
                            MessageBox.Show($"Table Name: {tableName}", "Table Selected");
                            MessageBox.Show("The CSV file must not include ID columns", "Warning");
                            DatabaseVyjezdUdalostiGateway databaseVyjezdUdalosti = new DatabaseVyjezdUdalostiGateway();
                            databaseVyjezdUdalosti.Import(new MainWindow().PromptForImportCsv() ??
                                                          throw new InvalidOperationException());
                            break;
                        case "vozidlatechnika":
                            MessageBox.Show($"Table Name: {tableName}", "Table Selected");
                            MessageBox.Show("The CSV file must not include ID columns", "Warning");
                            DatabaseVozidlaTechnikaGateway databaseVozidlaTechnika =
                                new DatabaseVozidlaTechnikaGateway();
                            databaseVozidlaTechnika.Import(
                                new MainWindow().PromptForImportCsv() ?? throw new InvalidOperationException());
                            break;
                        case "":
                            MessageBox.Show("No valid table has been selected", "Error");
                            break;
                        default:
                            MessageBox.Show("No valid table has been selected", "Error");
                            break;
                    }
                }
            }
            else
            {
                MessageBox.Show("No configuration file selected", "Error");
            }
        }
    }
}