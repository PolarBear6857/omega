﻿using System.Windows;
using Omega.DatabaseClasses.ClenoveSboru;
using Omega.DatabaseClasses.ClenoveSmeny;
using Omega.DatabaseClasses.Smeny;
using Omega.DatabaseClasses.VozidlaTechnika;
using Omega.DatabaseClasses.Vyjezd;
using Omega.DatabaseClasses.VyjezdUdalosti;

namespace Omega.OperationHandlers
{
    /// <summary>
    /// Class for inserting data into database tables.
    /// </summary>
    public class InsertHandler
    {
        /// <summary>
        /// Initializes a new instance of the Insert class.
        /// </summary>
        public InsertHandler()
        {
            if (MainWindow.ConfigurationFile)
            {
                string tableName = MainWindow.PromptForTableName();

                if (!string.IsNullOrEmpty(tableName))
                {
                    switch (tableName.ToLower())
                    {
                        case "clenovesmeny":
                            MessageBox.Show($"Table Name: {tableName}", "Table Selected");
                            ClenoveSmeny clenoveSmeny = new ClenoveSmeny();
                            clenoveSmeny.Show();
                            break;
                        case "smeny":
                            MessageBox.Show($"Table Name: {tableName}", "Table Selected");
                            Smeny smeny = new Smeny();
                            smeny.Show();
                            break;
                        case "clenovesboru":
                            MessageBox.Show($"Table Name: {tableName}", "Table Selected");
                            ClenoveSboru clenoveSboru = new ClenoveSboru();
                            clenoveSboru.Show();
                            break;
                        case "vyjezd":
                            MessageBox.Show($"Table Name: {tableName}", "Table Selected");
                            Vyjezd vyjezd = new Vyjezd();
                            vyjezd.Show();
                            break;
                        case "vyjezdudalosti":
                            MessageBox.Show($"Table Name: {tableName}", "Table Selected");
                            VyjezdUdalosti vyjezdUdalosti = new VyjezdUdalosti();
                            vyjezdUdalosti.Show();
                            break;
                        case "vozidlatechnika":
                            MessageBox.Show($"Table Name: {tableName}", "Table Selected");
                            VozidlaTechnika vozidlaTechnika = new VozidlaTechnika();
                            vozidlaTechnika.Show();
                            break;
                        case "":
                            MessageBox.Show("No valid table has been selected", "Error");
                            break;
                        default:
                            MessageBox.Show("No valid table has been selected", "Error");
                            break;
                    }
                }
            }
            else
            {
                MessageBox.Show("No configuration file selected", "Error");
            }
        }
    }
}