﻿using System;
using System.Data.SqlClient;
using System.IO;
using System.Windows;
using Omega.DatabaseClasses;
using Omega.DatabaseClasses.ClenoveSboru;
using Omega.DatabaseClasses.ClenoveSmeny;
using Omega.DatabaseClasses.Log;
using Omega.DatabaseClasses.Smeny;
using Omega.DatabaseClasses.VozidlaTechnika;
using Omega.DatabaseClasses.Vyjezd;
using Omega.DatabaseClasses.VyjezdUdalosti;

namespace Omega.OperationHandlers
{
    /// <summary>
    /// Class for exporting data from database tables and views.
    /// </summary>
    public class ExportHandler
    {
        /// <summary>
        /// Initializes a new instance of the Export class.
        /// </summary>
        public ExportHandler()
        {
            if (MainWindow.ConfigurationFile)
            {
                string tableName = MainWindow.PromptForTableName();

                if (!string.IsNullOrEmpty(tableName))
                {
                    switch (tableName.ToLower())
                    {
                        case "clenovesmeny":
                            MessageBox.Show($"Table Name: {tableName}", "Table Selected");
                            DatabaseClenoveSmenyGateway databaseClenoveSmeny = new DatabaseClenoveSmenyGateway();
                            databaseClenoveSmeny.Export();
                            break;
                        case "smeny":
                            MessageBox.Show($"Table Name: {tableName}", "Table Selected");
                            DatabaseSmenyGateway databaseSmeny = new DatabaseSmenyGateway();
                            databaseSmeny.Export();
                            break;
                        case "clenovesboru":
                            MessageBox.Show($"Table Name: {tableName}", "Table Selected");
                            DatabaseClenoveSboruGateway databaseClenoveSboru = new DatabaseClenoveSboruGateway();
                            databaseClenoveSboru.Export();
                            break;
                        case "vyjezd":
                            MessageBox.Show($"Table Name: {tableName}", "Table Selected");
                            DatabaseVyjezdGateway databaseVyjezd = new DatabaseVyjezdGateway();
                            databaseVyjezd.Export();
                            break;
                        case "vyjezdudalosti":
                            MessageBox.Show($"Table Name: {tableName}", "Table Selected");
                            DatabaseVyjezdUdalostiGateway databaseVyjezdUdalosti = new DatabaseVyjezdUdalostiGateway();
                            databaseVyjezdUdalosti.Export();
                            break;
                        case "vozidlatechnika":
                            MessageBox.Show($"Table Name: {tableName}", "Table Selected");
                            DatabaseVozidlaTechnikaGateway databaseVozidlaTechnika =
                                new DatabaseVozidlaTechnikaGateway();
                            databaseVozidlaTechnika.Export();
                            break;
                        case "log":
                            MessageBox.Show($"Table Name: {tableName}", "Table Selected");
                            DatabaseLogGateway databaseLogGateway = new DatabaseLogGateway();
                            databaseLogGateway.Export();
                            break;
                        case "vozidlatechnikaview":
                            MessageBox.Show($"View Name: {tableName}", "View Selected");
                            ExportViewVozidlaTechnika();
                            break;
                        case "clenovesboruview":
                            MessageBox.Show($"View Name: {tableName}", "View Selected");
                            ExportViewClenoveSboru();
                            break;
                        case "":
                            MessageBox.Show("No valid table has been selected", "Error");
                            break;
                        default:
                            MessageBox.Show("No valid table has been selected", "Error");
                            break;
                    }
                }
            }
            else
            {
                MessageBox.Show("No configuration file selected", "Error");
            }
        }

        /// <summary>
        /// Exports the VozidlaTechnika view to CSV.
        /// </summary>
        private void ExportViewVozidlaTechnika()
        {
            string filePath =
                MainWindow.ConfigurationFileLoader.Configuration
                    .VozidlaTechnikaViewExportPath;
            using DatabaseConnection d = DatabaseConnection.Instance;
            try
            {
                using SqlCommand command = new SqlCommand("SELECT * FROM View_VozidlaTechnika", d.Connection);
                try
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            using (StreamWriter writer = new StreamWriter(filePath))
                            {
                                // Write header (column names) to the file
                                for (int i = 0; i < reader.FieldCount; i++)
                                {
                                    string columnName = reader.GetName(i);
                                    writer.Write($"{columnName}");

                                    if (i < reader.FieldCount - 1)
                                    {
                                        // Add a comma unless it's the last column
                                        writer.Write(",");
                                    }
                                }

                                writer.WriteLine(); // Move to the next line after writing the header

                                // Iterate through the rows and write data to the file
                                while (reader.Read())
                                {
                                    for (int i = 0; i < reader.FieldCount; i++)
                                    {
                                        // Access columns by index or column name
                                        object columnValue = reader.GetValue(i);

                                        writer.Write($"{columnValue}");

                                        if (i < reader.FieldCount - 1)
                                        {
                                            // Add a comma unless it's the last column
                                            writer.Write(",");
                                        }
                                    }

                                    writer.WriteLine(); // Move to the next line after writing the row
                                }
                            }

                            MessageBox.Show($"Exported data to {filePath}", "Table Exported");
                        }
                        else
                        {
                            MessageBox.Show("No rows found", "Error");
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Error: {ex.Message}");
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("No view found", "Error");
            }
        }

        /// <summary>
        /// Exports the ClenoveSboru view to CSV.
        /// </summary>
        private void ExportViewClenoveSboru()
        {
            string filePath =
                MainWindow.ConfigurationFileLoader.Configuration
                    .ClenoveSboruViewExportPath;
            using DatabaseConnection d = DatabaseConnection.Instance;
            try
            {
                using (SqlCommand command = new SqlCommand("SELECT * FROM View_ClenoveSboru", d.Connection))
                {
                    try
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            if (reader.HasRows)
                            {
                                using (StreamWriter writer = new StreamWriter(filePath))
                                {
                                    // Write header (column names) to the file
                                    for (int i = 0; i < reader.FieldCount; i++)
                                    {
                                        string columnName = reader.GetName(i);
                                        writer.Write($"{columnName}");

                                        if (i < reader.FieldCount - 1)
                                        {
                                            // Add a comma unless it's the last column
                                            writer.Write(",");
                                        }
                                    }

                                    writer.WriteLine(); // Move to the next line after writing the header

                                    // Iterate through the rows and write data to the file
                                    while (reader.Read())
                                    {
                                        for (int i = 0; i < reader.FieldCount; i++)
                                        {
                                            // Access columns by index or column name
                                            object columnValue = reader.GetValue(i);

                                            writer.Write($"{columnValue}");

                                            if (i < reader.FieldCount - 1)
                                            {
                                                // Add a comma unless it's the last column
                                                writer.Write(",");
                                            }
                                        }

                                        writer.WriteLine(); // Move to the next line after writing the row
                                    }
                                }

                                MessageBox.Show($"Exported data to {filePath}", "Table Exported");
                            }
                            else
                            {
                                MessageBox.Show("No rows found", "Error");
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine($"Error: {ex.Message}");
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("No view found", "Error");
            }
        }
    }
}