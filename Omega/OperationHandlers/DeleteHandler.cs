﻿using System.Windows;
using Omega.DatabaseClasses.ClenoveSboru;
using Omega.DatabaseClasses.ClenoveSmeny;
using Omega.DatabaseClasses.Smeny;
using Omega.DatabaseClasses.VozidlaTechnika;
using Omega.DatabaseClasses.Vyjezd;
using Omega.DatabaseClasses.VyjezdUdalosti;

namespace Omega.OperationHandlers
{
    /// <summary>
    /// Class for deleting entries from various database tables.
    /// </summary>
    public class DeleteHandler
    {
        /// <summary>
        /// Initializes a new instance of the Delete class.
        /// </summary>
        public DeleteHandler()
        {
            if (MainWindow.ConfigurationFile)
            {
                string tableName = MainWindow.PromptForTableName();

                if (!string.IsNullOrEmpty(tableName))
                {
                    int id;
                    switch (tableName.ToLower())
                    {
                        case "clenovesmeny":
                            MessageBox.Show($"Table Name: {tableName}", "Table Selected");
                            DatabaseClenoveSmenyGateway databaseClenoveSmeny = new DatabaseClenoveSmenyGateway();
                            if (int.TryParse(Microsoft.VisualBasic.Interaction.InputBox(
                                    "Enter the id of the database entry:", "Table Id Input"), out id))
                            {
                                databaseClenoveSmeny.Delete(id);
                            }
                            else
                            {
                                MessageBox.Show("Some of the values are not correct", "Error");
                            }

                            break;
                        case "smeny":
                            MessageBox.Show($"Table Name: {tableName}", "Table Selected");
                            DatabaseSmenyGateway databaseSmeny = new DatabaseSmenyGateway();
                            if (int.TryParse(Microsoft.VisualBasic.Interaction.InputBox(
                                    "Enter the id of the database entry:", "Table Id Input"), out id))
                            {
                                databaseSmeny.Delete(id);
                            }
                            else
                            {
                                MessageBox.Show("Some of the values are not correct", "Error");
                            }

                            break;
                        case "clenovesboru":
                            MessageBox.Show($"Table Name: {tableName}", "Table Selected");
                            DatabaseClenoveSboruGateway databaseClenoveSboru = new DatabaseClenoveSboruGateway();
                            if (int.TryParse(Microsoft.VisualBasic.Interaction.InputBox(
                                    "Enter the id of the database entry:", "Table Id Input"), out id))
                            {
                                databaseClenoveSboru.Delete(id);
                            }
                            else
                            {
                                MessageBox.Show("Some of the values are not correct", "Error");
                            }

                            break;
                        case "vyjezd":
                            MessageBox.Show($"Table Name: {tableName}", "Table Selected");
                            DatabaseVyjezdGateway databaseVyjezd = new DatabaseVyjezdGateway();
                            if (int.TryParse(Microsoft.VisualBasic.Interaction.InputBox(
                                    "Enter the id of the database entry:", "Table Id Input"), out id))
                            {
                                databaseVyjezd.Delete(id);
                            }
                            else
                            {
                                MessageBox.Show("Some of the values are not correct", "Error");
                            }

                            break;
                        case "vyjezdudalosti":
                            MessageBox.Show($"Table Name: {tableName}", "Table Selected");
                            DatabaseVyjezdUdalostiGateway databaseVyjezdUdalosti = new DatabaseVyjezdUdalostiGateway();
                            if (int.TryParse(Microsoft.VisualBasic.Interaction.InputBox(
                                    "Enter the id of the database entry:", "Table Id Input"), out id))
                            {
                                databaseVyjezdUdalosti.Delete(id);
                            }
                            else
                            {
                                MessageBox.Show("Some of the values are not correct", "Error");
                            }

                            break;
                        case "vozidlatechnika":
                            MessageBox.Show($"Table Name: {tableName}", "Table Selected");
                            DatabaseVozidlaTechnikaGateway databaseVozidlaTechnika =
                                new DatabaseVozidlaTechnikaGateway();
                            if (int.TryParse(Microsoft.VisualBasic.Interaction.InputBox(
                                    "Enter the id of the database entry:", "Table Id Input"), out id))
                            {
                                databaseVozidlaTechnika.Delete(id);
                            }
                            else
                            {
                                MessageBox.Show("Some of the values are not correct", "Error");
                            }

                            break;
                        case "":
                            MessageBox.Show("No valid table has been selected", "Error");
                            break;
                        default:
                            MessageBox.Show("No valid table has been selected", "Error");
                            break;
                    }
                }
            }
            else
            {
                MessageBox.Show("No configuration file selected", "Error");
            }
        }
    }
}