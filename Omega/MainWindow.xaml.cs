﻿using System;
using System.Data.SqlClient;
using System.IO;
using System.Windows;
using Microsoft.Win32;
using Omega.lib;
using Omega.OperationHandlers;

namespace Omega
{
    /// <summary>
    /// Represents the main window of the Omega application.
    /// Architectures used: Single Responsibility Principle (SRP), Database Gateway
    /// </summary>
    public partial class MainWindow
    {
        public static bool ConfigurationFile;

        public static ConfigurationFileLoader ConfigurationFileLoader;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindow"/> class.
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();
            Loaded += MainWindow_Loaded;
        }

        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            string tmpPath = LoadLastConfigPath();
            if (tmpPath != "")
            {
                MessageBox.Show(
                    $"Configuration file was found. If you want to use another file, press the Config button.",
                    "File Found");
                ConfigurationFileLoader = new ConfigurationFileLoader(tmpPath);
                ConfigurationFile = true;
                LoadTableNames();
            }
            else
            {
                Console.WriteLine("Old config file not found.");
            }
        }

        private string LoadLastConfigPath()
        {
            string projectDirectory = AppDomain.CurrentDomain.BaseDirectory;
            Console.WriteLine(projectDirectory);
            string filePath = Path.Combine(projectDirectory, "lastconfigfile.txt");

            if (File.Exists(filePath))
            {
                string content = File.ReadAllText(filePath);
                return content;
            }

            return "";
        }

        private void SaveLastConfigPath(string path)
        {
            string projectDirectory = AppDomain.CurrentDomain.BaseDirectory;
            string filePath = Path.Combine(projectDirectory, "lastconfigfile.txt");
            Console.WriteLine($"Path of last config file saved here: {filePath}");
            File.WriteAllText(filePath, path);
        }

        /// <summary>
        /// Handles the event when the "Choose Configuration File" button is clicked.
        /// </summary>
        private void ChooseConfigurationFileButton_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog
            {
                Filter = "JSON Files (*.json)|*.json|All Files (*.*)|*.*",
                Title = "Select a File"
            };

            if (openFileDialog.ShowDialog() == true)
            {
                MessageBox.Show($"Selected File: {openFileDialog.FileName}", "File Selected");
                ConfigurationFileLoader = new ConfigurationFileLoader(openFileDialog.FileName);
                ConfigurationFile = true;
                SaveLastConfigPath(openFileDialog.FileName);
                LoadTableNames();
            }
        }

        /// <summary>
        /// Handles the event when the "Insert Into Table" button is clicked.
        /// </summary>
        private void InsertIntoTableButton_Click(object sender, RoutedEventArgs e)
        {
            _ = new InsertHandler();
        }

        /// <summary>
        /// Handles the event when the "Delete From Table" button is clicked.
        /// </summary>
        private void DeleteFromTableButton_Click(object sender, RoutedEventArgs e)
        {
            _ = new DeleteHandler();
        }

        /// <summary>
        /// Handles the event when the "Update Table" button is clicked.
        /// </summary>
        private void UpdateTableButton_Click(object sender, RoutedEventArgs e)
        {
            _ = new UpdateHandler();
        }

        /// <summary>
        /// Handles the event when the "Export Table" button is clicked.
        /// </summary>
        private void ExportTableToFile_Click(object sender, RoutedEventArgs e)
        {
            _ = new ExportHandler();
        }

        /// <summary>
        /// Handles the event when the "Import Into Table" button is clicked.
        /// </summary>
        private void ImportIntoTableFromCSV_Click(object sender, RoutedEventArgs e)
        {
            _ = new ImportHandler();
        }

        /// <summary>
        /// Prompts the user to enter the name of the database table.
        /// </summary>
        /// <returns>The name of the database table entered by the user.</returns>
        public static string PromptForTableName()
        {
            return Microsoft.VisualBasic.Interaction.InputBox("Enter the name of the database table:",
                "Table Name Input");
        }

        /// <summary>
        /// Prompts the user to select a CSV file for import.
        /// </summary>
        /// <returns>The path of the selected CSV file for import, or null if not selected.</returns>
        public string? PromptForImportCsv()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog
            {
                Filter = "CSV Files (*.csv)|*.csv|All Files (*.*)|*.*",
                Title = "Select a File"
            };

            if (openFileDialog.ShowDialog() == true)
            {
                return openFileDialog.FileName;
            }

            return null;
        }

        /// <summary>
        /// Loads all tables names from database.
        /// </summary>
        private void LoadTableNames()
        {
            using SqlConnection connection =
                new SqlConnection(ConfigurationFileLoader.Configuration.DatabaseConnectionString);
            try
            {
                connection.Open();
            }
            catch
            {
                MessageBox.Show("Connection string is not correct", "Error");
                return;
            }


            string query = "SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE = 'BASE TABLE'";

            SqlCommand command = new SqlCommand(query, connection);
            SqlDataReader reader = command.ExecuteReader();

            TextBoxTables.Clear();

            while (reader.Read())
            {
                string tableName = reader.GetString(0);
                if (!tableName.Equals("sysdiagrams"))
                {
                    TextBoxTables.AppendText(tableName + "\n");
                }
            }

            reader.Close();
        }
    }
}