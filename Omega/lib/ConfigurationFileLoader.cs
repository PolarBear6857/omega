﻿using System;
using System.IO;
using Newtonsoft.Json;

namespace Omega.lib
{
    /// <summary>
    /// Represents the configuration settings for the application.
    /// </summary>
    public class Configuration
    {
        /// <summary>
        /// Gets or sets the connection string for the database.
        /// </summary>
        public string DatabaseConnectionString { get; set; } = string.Empty;

        /// <summary>
        /// Gets or sets the export path for ClenoveSboru.
        /// </summary>
        public string ClenoveSboruExportPath { get; set; } = string.Empty;

        /// <summary>
        /// Gets or sets the export path for ClenoveSmeny.
        /// </summary>
        public string ClenoveSmenyExportPath { get; set; } = string.Empty;

        /// <summary>
        /// Gets or sets the export path for LogTabulka.
        /// </summary>
        public string LogTabulkaExportPath { get; set; } = string.Empty;

        /// <summary>
        /// Gets or sets the export path for Smeny.
        /// </summary>
        public string SmenyExportPath { get; set; } = string.Empty;

        /// <summary>
        /// Gets or sets the export path for VozidlaTechnika.
        /// </summary>
        public string VozidlaTechnikaExportPath { get; set; } = string.Empty;

        /// <summary>
        /// Gets or sets the export path for Vyjezd.
        /// </summary>
        public string VyjezdExportPath { get; set; } = string.Empty;

        /// <summary>
        /// Gets or sets the export path for VyjezdUdalosti.
        /// </summary>
        public string VyjezdUdalostiExportPath { get; set; } = string.Empty;

        /// <summary>
        /// Gets or sets the export path for VozidlaTechnikaView.
        /// </summary>
        public string VozidlaTechnikaViewExportPath { get; set; } = string.Empty;

        /// <summary>
        /// Gets or sets the export path for ClenoveSboruView.
        /// </summary>
        public string ClenoveSboruViewExportPath { get; set; } = string.Empty;
    }

    /// <summary>
    /// Loads configuration settings from a JSON file.
    /// </summary>
    public class ConfigurationFileLoader
    {
        /// <summary>
        /// Gets the loaded configuration settings.
        /// </summary>
        public readonly Configuration Configuration;

        /// <summary>
        /// Initializes a new instance of the ConfigurationFileLoader class.
        /// </summary>
        /// <param name="file">The path to the JSON file containing configuration settings.</param>
        public ConfigurationFileLoader(string file)
        {
            try
            {
                string jsonFilePath = file;

                if (!File.Exists(jsonFilePath))
                {
                    throw new FileNotFoundException("Configuration file not found.", jsonFilePath);
                }

                string jsonContent = File.ReadAllText(jsonFilePath);

                Configuration = JsonConvert.DeserializeObject<Configuration>(jsonContent) ?? new Configuration();

                if (Configuration.DatabaseConnectionString == string.Empty)
                {
                    throw new InvalidOperationException("Failed to deserialize configuration.");
                }

                // DEBUG
                Console.WriteLine($"Database Connection String: {Configuration.DatabaseConnectionString}");
                Console.WriteLine($"Clenove Sboru Export Path: {Configuration.ClenoveSboruExportPath}");
                Console.WriteLine($"Clenove Smeny Export Path: {Configuration.ClenoveSmenyExportPath}");
                Console.WriteLine($"Log Export Path: {Configuration.LogTabulkaExportPath}");
                Console.WriteLine($"Smeny Export Path: {Configuration.SmenyExportPath}");
                Console.WriteLine($"Vozidla Technika Export Path: {Configuration.VozidlaTechnikaExportPath}");
                Console.WriteLine($"Vyjezd Export Path: {Configuration.VyjezdExportPath}");
                Console.WriteLine($"Vyjezd Udalosti Export Path: {Configuration.VyjezdUdalostiExportPath}");
                Console.WriteLine($"Vozidla Technika View Export Path: {Configuration.VozidlaTechnikaViewExportPath}");
                Console.WriteLine($"Clenove Sboru View Export Path: {Configuration.ClenoveSboruViewExportPath}");
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error loading configuration: {ex.Message}");
                Configuration = new Configuration();
            }
        }
    }
}