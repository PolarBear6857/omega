﻿using System;
using System.Windows;

namespace Omega.DatabaseClasses.Vyjezd
{
    /// <summary>
    /// Window for updating Vyjezd information.
    /// </summary>
    public partial class VyjezdUpdate : Window
    {
        private DatabaseVyjezdGateway _d;

        /// <summary>
        /// Initializes a new instance of the VyjezdUpdate class.
        /// </summary>
        public VyjezdUpdate()
        {
            InitializeComponent();
            _d = new DatabaseVyjezdGateway();
            LoadDataGrid();
        }

        /// <summary>
        /// Event handler for the insert button click.
        /// </summary>
        /// <param name="sender">The object that triggered the event.</param>
        /// <param name="e">The event arguments.</param>
        private void UpdateTableButton_Click(object sender, RoutedEventArgs e)
        {
            string update = "UPDATE Vyjezd SET ";

            DateTime startDate = default;
            if (DatePickerResponse.SelectedDate != null && ResponseUpdateBool.IsChecked == false)
            {
                startDate = DatePickerResponse.SelectedDate ?? DateTime.MinValue;
                update += "DatumCasUdalosti = '" + startDate.ToString("yyyy-MM-dd") + "', ";
            }

            string mistoUdalosti = TextBoxPlace.Text;
            if (mistoUdalosti != "" && PlaceUpdateBool.IsChecked == false)
            {
                update += "TypVozidla = '" + TextBoxPlace.Text + "', ";
            }

            if (int.TryParse(TextBoxUdalost.Text, out _) && UdalostUpdateBool.IsChecked == false)
            {
                update += "TypVozidla = '" + TextBoxUdalost.Text + "', ";
            }

            if (int.TryParse(TextBoxVehicle.Text, out _) && VehicleUpdateBool.IsChecked == false)
            {
                update += "TypVozidla = '" + TextBoxVehicle.Text + "', ";
            }

            if (int.TryParse(TextBoxSmena.Text, out _) && SmenaUpdateBool.IsChecked == false)
            {
                update += "TypVozidla = '" + TextBoxSmena.Text + "', ";
            }

            if (!string.IsNullOrEmpty(update) && TextBoxID.Text != "")
            {
                if (int.TryParse(TextBoxID.Text, out _))
                {
                    update = update.Remove(update.Length - 2);
                    update += " WHERE IDVyjezd = " + TextBoxID.Text;
                    _d.Update(update);
                    Close();
                }
                else
                {
                    MessageBox.Show("ID is not correct", "Error");
                }
            }
            else
            {
                MessageBox.Show("Some of the values are not correct", "Error");
            }
        }
        
        private void LoadDataGrid()
        {
            datagridVyjezd.ItemsSource = _d.Select().DefaultView;
        }
    }
}