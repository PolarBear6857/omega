﻿using System;
using System.Windows;

namespace Omega.DatabaseClasses.Vyjezd
{
    /// <summary>
    /// Represents a window for handling interactions with the "Vyjezd" table in the database.
    /// </summary>
    public partial class Vyjezd : Window
    {
        private DatabaseVyjezdGateway _d;

        /// <summary>
        /// Initializes a new instance of the <see cref="Vyjezd"/> class.
        /// </summary>
        public Vyjezd()
        {
            InitializeComponent();
            _d = new DatabaseVyjezdGateway();
        }

        /// <summary>
        /// Handles the button click event to insert a new record into the "Vyjezd" table.
        /// </summary>
        /// <param name="sender">The object that raised the event.</param>
        /// <param name="e">The event arguments.</param>
        private void InsertIntoTableButton_Click(object sender, RoutedEventArgs e)
        {
            int numberofcorrectinputs = 0;

            DateTime startDate = default;
            if (DatePickerResponse.SelectedDate != null)
            {
                startDate = DatePickerResponse.SelectedDate ?? DateTime.MinValue;
                numberofcorrectinputs++;
            }

            string mistoUdalosti = TextBoxPlace.Text;
            if (mistoUdalosti != "")
            {
                numberofcorrectinputs++;
            }

            int udalost;
            if (int.TryParse(TextBoxUdalost.Text, out udalost))
            {
                numberofcorrectinputs++;
            }

            int vehicle;
            if (int.TryParse(TextBoxVehicle.Text, out vehicle))
            {
                numberofcorrectinputs++;
            }

            int smena;
            if (int.TryParse(TextBoxSmena.Text, out smena))
            {
                numberofcorrectinputs++;
            }

            if (numberofcorrectinputs == 5)
            {
                _d.Insert(startDate, mistoUdalosti, udalost, vehicle, smena);
                Close();
            }
            else
            {
                MessageBox.Show("Some of the values are not correct", "Error");
            }
        }
    }
}