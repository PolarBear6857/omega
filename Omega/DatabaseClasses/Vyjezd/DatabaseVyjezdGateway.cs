﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Windows;
using Microsoft.VisualBasic.FileIO;

namespace Omega.DatabaseClasses.Vyjezd
{
    /// <summary>
    /// Represents a gateway for interacting with the "Vyjezd" table in the database.
    /// </summary>
    public class DatabaseVyjezdGateway
    {
        /// <summary>
        /// Inserts a new record into the "Vyjezd" table.
        /// </summary>
        /// <param name="startDate">The start date of the event.</param>
        /// <param name="mistoUdalosti">The location of the event.</param>
        /// <param name="udalost">The ID of the event.</param>
        /// <param name="vehicle">The ID of the vehicle associated with the event.</param>
        /// <param name="smena">The ID of the shift associated with the event.</param>
        public void Insert(DateTime startDate, string mistoUdalosti, int udalost, int vehicle, int smena)
        {
            using DatabaseConnection d = DatabaseConnection.Instance;
            using SqlCommand command =
                new SqlCommand("INSERT INTO Vyjezd VALUES (@value1, @value2, @value3, @value4, @value5)",
                    d.Connection);
            command.Parameters.AddWithValue("@value1", startDate);
            command.Parameters.AddWithValue("@value2", mistoUdalosti);
            command.Parameters.AddWithValue("@value3", udalost);
            command.Parameters.AddWithValue("@value4", vehicle);
            command.Parameters.AddWithValue("@value5", smena);

            try
            {
                // Execute the query
                int rowsAffected = command.ExecuteNonQuery();
                Console.WriteLine($"Rows affected: {rowsAffected}");
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error: {ex.Message}");
            }
        }

        /// <summary>
        /// Deletes a record from the "Vyjezd" table based on the specified ID.
        /// </summary>
        /// <param name="id">The ID of the record to delete.</param>
        public void Delete(int id)
        {
            using DatabaseConnection d = DatabaseConnection.Instance;
            using SqlCommand command =
                new SqlCommand("DELETE FROM Vyjezd WHERE IDVyjezd = @valeu1;", d.Connection);
            command.Parameters.AddWithValue("@value1", id);
            try
            {
                // Execute the query
                int rowsAffected = command.ExecuteNonQuery();
                Console.WriteLine($"Rows affected: {rowsAffected}");
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error: {ex.Message}");
            }
        }

        /// <summary>
        /// Exports data from the "Vyjezd" table to a CSV file.
        /// </summary>
        public void Export()
        {
            string filePath =
                MainWindow.ConfigurationFileLoader.Configuration
                    .VyjezdExportPath; // "C:\\Users\\Martin\\Desktop\\VyjezdExport.csv"
            using DatabaseConnection d = DatabaseConnection.Instance;
            using SqlCommand command = new SqlCommand("SELECT * FROM Vyjezd", d.Connection);
            try
            {
                using SqlDataReader reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    using (StreamWriter writer = new StreamWriter(filePath))
                    {
                        // Write header (column names) to the file
                        for (int i = 0; i < reader.FieldCount; i++)
                        {
                            string columnName = reader.GetName(i);
                            writer.Write($"{columnName}");

                            if (i < reader.FieldCount - 1)
                            {
                                // Add a comma unless it's the last column
                                writer.Write(",");
                            }
                        }

                        writer.WriteLine(); // Move to the next line after writing the header

                        // Iterate through the rows and write data to the file
                        while (reader.Read())
                        {
                            for (int i = 0; i < reader.FieldCount; i++)
                            {
                                // Access columns by index or column name
                                object columnValue = reader.GetValue(i);

                                writer.Write($"{columnValue}");

                                if (i < reader.FieldCount - 1)
                                {
                                    // Add a comma unless it's the last column
                                    writer.Write(",");
                                }
                            }

                            writer.WriteLine(); // Move to the next line after writing the row
                        }
                    }

                    MessageBox.Show($"Exported data to {filePath}", "Table Exported");
                }
                else
                {
                    MessageBox.Show("No rows found", "Error");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error: {ex.Message}");
            }
        }

        /// <summary>
        /// Imports data from a CSV file into the "Vyjezd" table.
        /// </summary>
        /// <param name="filePath">The path to the CSV file to import.</param>
        public void Import(string filePath)
        {
            using DatabaseConnection d = DatabaseConnection.Instance;
            try
            {
                using TextFieldParser parser = new TextFieldParser(filePath);
                parser.Delimiters = new string[] { "," };
                string[] columns = parser.ReadFields();
                string insertQuery = $"INSERT INTO Vyjezd ({string.Join(", ", columns)}) VALUES ";

                while (!parser.EndOfData)
                {
                    string[] fields = parser.ReadFields();

                    fields = fields.Where(field => !string.IsNullOrEmpty(field)).ToArray();

                    if (DateTime.TryParseExact(fields[0], "yyyy-MM-dd HH:mm:ss", null,
                            System.Globalization.DateTimeStyles.None, out DateTime parsedDateTime))
                    {
                        fields[0] = parsedDateTime.ToString("yyyy-MM-dd HH:mm:ss");
                    }
                    else
                    {
                        Console.WriteLine($"Error: Invalid datetime format for value '{fields[0]}'");
                        continue;
                    }

                    string values = $"('{string.Join("', '", fields)}')";

                    Console.WriteLine(insertQuery + values);

                    using SqlCommand command = new SqlCommand(insertQuery + values, d.Connection);
                    command.ExecuteNonQuery();
                }

                MessageBox.Show($"Imported data from {filePath} to the database", "Table Imported");
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error: {ex.Message}");
            }
        }

        /// <summary>
        /// Updates data in the "Vyjezd" table based on the provided ID.
        /// </summary>
        /// <param name="updateCommand"></param>
        public void Update(string updateCommand)
        {
            using DatabaseConnection d = DatabaseConnection.Instance;
            try
            {
                using SqlCommand command = new SqlCommand(updateCommand, d.Connection);
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error: {ex.Message}");
            }
        }

        /// <summary>
        /// Selects all data from "Vyjezd" table.
        /// </summary>
        /// <returns>Returns all table records.</returns>
        public DataTable Select()
        {
            using DatabaseConnection d = DatabaseConnection.Instance;
            string query = "SELECT * FROM Vyjezd";
            SqlCommand command = new SqlCommand(query, d.Connection);
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            DataTable dataTable = new DataTable();
            adapter.Fill(dataTable);
            return dataTable;
        }
    }
}