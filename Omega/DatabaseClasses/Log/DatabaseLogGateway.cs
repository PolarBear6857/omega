﻿using System;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Windows;

namespace Omega.DatabaseClasses.Log
{
    /// <summary>
    /// Represents a gateway for interacting with the "LogTabulka" table in the database.
    /// </summary>
    public class DatabaseLogGateway
    {
        private const string LogFilePath = "error_log.txt";

        /// <summary>
        /// Exports data from the "LogTabulka" table to a CSV file.
        /// </summary>
        public void Export()
        {
            string filePath = MainWindow.ConfigurationFileLoader.Configuration.LogTabulkaExportPath;

            using var databaseConnection = DatabaseConnection.Instance;

            if (databaseConnection.Connection == null)
            {
                ShowError("Database connection is not established.");
                return;
            }

            var selectQuery = "SELECT * FROM LogTabulka";

            using var sqlCommand = new SqlCommand(selectQuery, databaseConnection.Connection);

            try
            {
                using var reader = sqlCommand.ExecuteReader();

                if (reader.HasRows)
                {
                    using var writer = new StreamWriter(filePath, false, Encoding.UTF8);

                    WriteHeader(writer, reader);

                    WriteRows(writer, reader);

                    ShowSuccess($"Exported data to {filePath}");
                }
                else
                {
                    ShowError("No rows found");
                }
            }
            catch (Exception ex)
            {
                LogError(ex);
                ShowError($"Error occurred: {ex.Message}");
            }
        }

        private void WriteHeader(StreamWriter writer, SqlDataReader reader)
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                string columnName = reader.GetName(i);
                WriteCsvField(writer, columnName);

                if (i < reader.FieldCount - 1)
                {
                    writer.Write(",");
                }
            }

            writer.WriteLine();
        }

        private void WriteRows(StreamWriter writer, SqlDataReader reader)
        {
            while (reader.Read())
            {
                for (int i = 0; i < reader.FieldCount; i++)
                {
                    object columnValue = reader.GetValue(i);
                    WriteCsvField(writer, columnValue);

                    if (i < reader.FieldCount - 1)
                    {
                        writer.Write(",");
                    }
                }

                writer.WriteLine();
            }
        }

        private void WriteCsvField(StreamWriter writer, object value)
        {
            if (value == null || value == DBNull.Value)
            {
                return;
            }

            string stringValue = value.ToString();
            if (stringValue.Contains(",") || stringValue.Contains("\"") || stringValue.Contains("\n"))
            {
                stringValue = stringValue.Replace("\"", "\"\"");
                stringValue = $"\"{stringValue}\"";
            }

            writer.Write(stringValue);
        }

        private void ShowError(string message)
        {
            MessageBox.Show(message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
        }

        private void ShowSuccess(string message)
        {
            MessageBox.Show(message, "Success", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        private void LogError(Exception ex)
        {
            try
            {
                using StreamWriter logWriter = new StreamWriter(LogFilePath, true);
                logWriter.WriteLine($"[{DateTime.Now}] Error: {ex.Message}");
                logWriter.WriteLine($"StackTrace: {ex.StackTrace}");
                logWriter.WriteLine();
            }
            catch (IOException)
            {
                Console.WriteLine();
            }
        }
    }
}