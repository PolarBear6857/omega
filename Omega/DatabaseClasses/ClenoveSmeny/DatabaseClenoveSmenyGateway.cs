﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Windows;
using Microsoft.VisualBasic.FileIO;

namespace Omega.DatabaseClasses.ClenoveSmeny
{
    /// <summary>
    /// Represents a gateway for interacting with the "ClenoveSmeny" table in the database.
    /// </summary>
    public class DatabaseClenoveSmenyGateway
    {
        /// <summary>
        /// Inserts a new record into the "ClenoveSmeny" table with the provided information.
        /// </summary>
        /// <param name="idSmeny">The ID of the shift.</param>
        /// <param name="idClena">The ID of the member.</param>
        public void Insert(int idSmeny, int idClena)
        {
            using DatabaseConnection d = DatabaseConnection.Instance;
            using SqlCommand command =
                new SqlCommand("INSERT INTO ClenoveSmeny VALUES (@value1, @value2)", d.Connection);
            command.Parameters.AddWithValue("@value1", idSmeny);
            command.Parameters.AddWithValue("@value2", idClena);
            try
            {
                // Execute the query
                int rowsAffected = command.ExecuteNonQuery();
                Console.WriteLine($"Rows affected: {rowsAffected}");
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error: {ex.Message}");
            }
        }

        /// <summary>
        /// Deletes a record from the "ClenoveSmeny" table based on the provided ID.
        /// </summary>
        /// <param name="id">The ID of the record to be deleted.</param>
        public void Delete(int id)
        {
            using DatabaseConnection d = DatabaseConnection.Instance;
            using SqlCommand command =
                new SqlCommand("DELETE FROM ClenoveSmeny WHERE ID = @valeu1;", d.Connection);
            command.Parameters.AddWithValue("@value1", id);
            try
            {
                // Execute the query
                int rowsAffected = command.ExecuteNonQuery();
                Console.WriteLine($"Rows affected: {rowsAffected}");
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error: {ex.Message}");
            }
        }

        /// <summary>
        /// Exports data from the "ClenoveSmeny" table to a CSV file.
        /// </summary>
        public void Export()
        {
            string filePath =
                MainWindow.ConfigurationFileLoader.Configuration
                    .ClenoveSmenyExportPath;
            using DatabaseConnection d = DatabaseConnection.Instance;
            using SqlCommand command = new SqlCommand("SELECT * FROM ClenoveSmeny", d.Connection);
            try
            {
                using SqlDataReader reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    using (StreamWriter writer = new StreamWriter(filePath))
                    {
                        for (int i = 0; i < reader.FieldCount; i++)
                        {
                            string columnName = reader.GetName(i);
                            writer.Write($"{columnName},");
                        }

                        writer.WriteLine();

                        while (reader.Read())
                        {
                            for (int i = 0; i < reader.FieldCount; i++)
                            {
                                object columnValue = reader.GetValue(i);

                                writer.Write($"{columnValue},");
                            }

                            writer.WriteLine();
                        }
                    }

                    MessageBox.Show($"Exported data to {filePath}", "Table Exported");
                }
                else
                {
                    MessageBox.Show("No rows found", "Error");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error: {ex.Message}");
            }
        }

        /// <summary>
        /// Imports data from a CSV file into the "ClenoveSmeny" table.
        /// </summary>
        /// <param name="filePath">The path of the CSV file to be imported.</param>
        public void Import(string filePath)
        {
            using DatabaseConnection d = DatabaseConnection.Instance;
            try
            {
                // Create a TextFieldParser to read the CSV file
                using TextFieldParser parser = new TextFieldParser(filePath);
                parser.Delimiters = new string[] { "," };

                // Read the header (column names)
                string[]? columns = parser.ReadFields();

                // Assume the first row contains column names; adjust if necessary
                // This example assumes that the CSV file has the same structure as the table
                // You may need to map CSV columns to database columns if they are different
                string insertQuery = $"INSERT INTO ClenoveSmeny ({string.Join(", ", columns)}) VALUES ";
                
                // Read and insert data from each row
                while (!parser.EndOfData)
                {
                    Console.WriteLine("clenovesmeny");
                    string[]? fields = parser.ReadFields();

                    // Build the VALUES part of the SQL INSERT query
                    if (fields != null)
                    {
                        string values = $"('{string.Join("', '", fields)}')";

                        // Execute the INSERT query
                        using SqlCommand command = new SqlCommand(insertQuery + values, d.Connection);
                        command.ExecuteNonQuery();
                    }
                }

                MessageBox.Show($"Imported data from {filePath} to the database", "Table Imported");
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error: {ex.Message}");
            }
        }

        /// <summary>
        /// Updates data in the "ClenoveSmeny" table based on the provided ID.
        /// </summary>
        /// <param name="updateCommand"></param>
        public void Update(string updateCommand)
        {
            using DatabaseConnection d = DatabaseConnection.Instance;
            try
            {
                using SqlCommand command = new SqlCommand(updateCommand, d.Connection);
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error: {ex.Message}");
            }
        }
        
        /// <summary>
        /// Selects all data from "ClenoveSmeny" table.
        /// </summary>
        /// <returns>Returns all table records.</returns>
        public DataTable Select()
        {
            using DatabaseConnection d = DatabaseConnection.Instance;
            string query = "SELECT * FROM ClenoveSmeny";
            SqlCommand command = new SqlCommand(query, d.Connection);
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            DataTable dataTable = new DataTable();
            adapter.Fill(dataTable);
            return dataTable;
        }
    }
}