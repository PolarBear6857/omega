﻿using System.Windows;

namespace Omega.DatabaseClasses.ClenoveSmeny
{
    /// <summary>
    /// Represents the window for managing the assignment of members to shifts.
    /// </summary>
    public partial class ClenoveSmeny : Window
    {
        private DatabaseClenoveSmenyGateway _d;

        /// <summary>
        /// Initializes a new instance of the <see cref="ClenoveSmeny"/> class.
        /// </summary>
        public ClenoveSmeny()
        {
            InitializeComponent();

            _d = new DatabaseClenoveSmenyGateway();
        }

        /// <summary>
        /// Handles the event when the "Insert Into Table" button is clicked.
        /// Inserts a new assignment of a member to a shift into the database.
        /// </summary>
        /// <param name="sender">The object that triggered the event.</param>
        /// <param name="e">The event arguments.</param>
        public void InsertIntoTableButton_Click(object sender, RoutedEventArgs e)
        {
            int numberofcorrectinputs = 0;
            
            int idSmena;
            if (int.TryParse(TextBoxSmena.Text, out idSmena))
            {
                numberofcorrectinputs++;
            }

            int idClena;
            if (int.TryParse(TextBoxClen.Text, out idClena))
            {
                numberofcorrectinputs++;
            }

            if (numberofcorrectinputs == 2)
            {
                _d.Insert(idSmena, idClena);
                Close();
            }
            else
            {
                MessageBox.Show("Some of the values are not correct", "Error");
            }
        }
    }
}