﻿using System.Windows;

namespace Omega.DatabaseClasses.ClenoveSmeny
{
    /// <summary>
    /// Window for updating ClenoveSmeny information.
    /// </summary>
    public partial class ClenoveSmenyUpdate : Window
    {
        private DatabaseClenoveSmenyGateway _d;

        /// <summary>
        /// Initializes a new instance of the ClenoveSmenyUpdate class.
        /// </summary>
        public ClenoveSmenyUpdate()
        {
            InitializeComponent();
            _d = new DatabaseClenoveSmenyGateway();
            LoadDataGrid();
        }

        /// <summary>
        /// Event handler for the update button click.
        /// </summary>
        /// <param name="sender">The object that triggered the event.</param>
        /// <param name="e">The event arguments.</param>
        public void UpdateTableButton_Click(object sender, RoutedEventArgs e)
        {
            string update = "UPDATE ClenoveSmeny SET ";

            if (int.TryParse(TextBoxSmena.Text, out _) && SmenaUpdateBool.IsChecked == false)
            {
                update += " IDSmeny = '" + TextBoxSmena.Text + "', ";
            }

            if (int.TryParse(TextBoxClen.Text, out _) && ClenUpdateBool.IsChecked == false)
            {
                update += " IDClena = '" + TextBoxClen.Text + "', ";
            }

            if (!string.IsNullOrEmpty(update) && TextBoxID.Text != "")
            {
                if (int.TryParse(TextBoxID.Text, out _))
                {
                    update = update.Remove(update.Length - 2);
                    update += " WHERE ID = " + TextBoxID.Text;
                    _d.Update(update);
                    Close();
                }
                else
                {
                    MessageBox.Show("ID is not correct", "Error");
                }
            }
            else
            {
                MessageBox.Show("Some of the values are not correct", "Error");
            }
        }
        
        private void LoadDataGrid()
        {
            datagridSmeny.ItemsSource = _d.Select().DefaultView;
        }
    }
}