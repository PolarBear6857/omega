﻿using System;
using System.Data.SqlClient;

namespace Omega.DatabaseClasses
{
    /// <summary>
    /// Represents a database connection for interacting with the "HZS_CR" database.
    /// </summary>
    public class DatabaseConnection : IDisposable
    {
        /// <summary>
        /// Represents a singleton instance of the database connection.
        /// </summary>
        public static DatabaseConnection Instance { get; } = new();

        private SqlConnection? _connection;


        /// <summary>
        /// Gets the SqlConnection associated with this DatabaseConnection instance.
        /// </summary>
        public SqlConnection? Connection
        {
            get
            {
                if (_connection != null) return _connection;
                _connection =
                    new SqlConnection(MainWindow.ConfigurationFileLoader.Configuration.DatabaseConnectionString);
                _connection.Open();

                return _connection;
            }
        }

        // Private constructor to prevent instantiation outside of the class.
        private DatabaseConnection()
        {
            try
            {
                _connection =
                    new SqlConnection(MainWindow.ConfigurationFileLoader.Configuration.DatabaseConnectionString);
                _connection.Open();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error: {ex.Message}");
                throw;
            }
        }

        /// <summary>
        /// Disposes of the DatabaseConnection instance and releases any associated resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Disposes of the DatabaseConnection instance and releases any associated resources.
        /// </summary>
        /// <param name="disposing">True if called explicitly, false if called from the finalizer.</param>
        protected virtual void Dispose(bool disposing)
        {
            if (!disposing) return;
            if (_connection == null) return;
            _connection.Dispose();
            _connection = null;
        }

        ~DatabaseConnection()
        {
            Dispose(false);
        }
    }
}