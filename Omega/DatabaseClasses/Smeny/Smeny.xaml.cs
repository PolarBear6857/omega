﻿using System;
using System.Windows;

namespace Omega.DatabaseClasses.Smeny
{
    /// <summary>
    /// Represents a window for interacting with the "Smeny" table in the database.
    /// </summary>
    public partial class Smeny : Window
    {
        private DatabaseSmenyGateway _d;

        /// <summary>
        /// Initializes a new instance of the <see cref="Smeny"/> class.
        /// </summary>
        public Smeny()
        {
            InitializeComponent();
            _d = new DatabaseSmenyGateway();
        }

        /// <summary>
        /// Handles the click event of the "InsertIntoTableButton" to insert a new record into the "Smeny" table.
        /// </summary>
        /// <param name="sender">The sender of the event.</param>
        /// <param name="e">The event arguments.</param>
        private void InsertIntoTableButton_Click(object sender, RoutedEventArgs e)
        {
            int numberofcorrectinputs = 0;

            DateTime startDate = default;
            if (DatePickerStartDate.SelectedDate != null)
            {
                startDate = DatePickerStartDate.SelectedDate ?? DateTime.MinValue;
                numberofcorrectinputs++;
            }

            DateTime endDate = default;
            if (DatePickerEndDate.SelectedDate != null)
            {
                endDate = DatePickerEndDate.SelectedDate ?? DateTime.MinValue;
                numberofcorrectinputs++;
            }

            string description = TextBoxDescription.Text;
            if (description != "")
            {
                numberofcorrectinputs++;
            }

            if (numberofcorrectinputs == 3)
            {
                _d.Insert(startDate, endDate, description);
                Close();
            }
            else
            {
                MessageBox.Show("Some of the values are not correct", "Error");
            }
        }
    }
}