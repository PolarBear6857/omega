﻿using System;
using System.Windows;

namespace Omega.DatabaseClasses.Smeny
{
    /// <summary>
    /// Window for updating shifts information.
    /// </summary>
    public partial class SmenyUpdate : Window
    {
        private DatabaseSmenyGateway _d;

        /// <summary>
        /// Initializes a new instance of the SmenyUpdate class.
        /// </summary>
        public SmenyUpdate()
        {
            InitializeComponent();
            _d = new DatabaseSmenyGateway();
            LoadDataGrid();
        }

        /// <summary>
        /// Event handler for the update button click.
        /// </summary>
        /// <param name="sender">The object that triggered the event.</param>
        /// <param name="e">The event arguments.</param>
        private void UpdateTableButton_Click(object sender, RoutedEventArgs e)
        {
            string update = "UPDATE Smeny SET ";

            DateTime startDate = default;
            if (DatePickerStartDate.SelectedDate != null && StartUpdateBool.IsChecked == false)
            {
                startDate = DatePickerStartDate.SelectedDate ?? DateTime.MinValue;
                update += "DatumCasZacatku = '" + startDate.ToString("yyyy-MM-dd") + "', ";
            }

            DateTime endDate = default;
            if (DatePickerEndDate.SelectedDate != null && EndUpdateBool.IsChecked == false)
            {
                endDate = DatePickerEndDate.SelectedDate ?? DateTime.MinValue;
                update += "DatumCasKonce = '" + endDate.ToString("yyyy-MM-dd") + "', ";
            }

            if (TextBoxDescription.Text != "" && DescUpdateBool.IsChecked == false)
            {
                update += "Popis = '" + TextBoxDescription.Text + "', ";
            }

            if (!string.IsNullOrEmpty(update) && TextBoxID.Text != "")
            {
                if (int.TryParse(TextBoxID.Text, out _))
                {
                    update = update.Remove(update.Length - 2);
                    update += " WHERE IDSmeny = " + TextBoxID.Text;
                    _d.Update(update);
                    Close();
                }
                else
                {
                    MessageBox.Show("ID is not correct", "Error");
                }
            }
            else
            {
                MessageBox.Show("Some of the values are not correct", "Error");
            }
        }
        
        private void LoadDataGrid()
        {
            datagridSmeny.ItemsSource = _d.Select().DefaultView;
        }
    }
}