﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using Microsoft.VisualBasic.FileIO;

namespace Omega.DatabaseClasses.ClenoveSboru
{
    /// <summary>
    /// Represents a gateway for interacting with the "ClenoveSboru" table in the database.
    /// </summary>
    public class DatabaseClenoveSboruGateway
    {
        private const string LogFilePath = "error_log.txt";

        /// <summary>
        /// Inserts a new record into the "ClenoveSboru" table with the provided information.
        /// </summary>
        /// <param name="jmeno">The first name of the member.</param>
        /// <param name="prijmeni">The last name of the member.</param>
        /// <param name="datumNarozeni">The date of birth of the member.</param>
        /// <param name="adresa">The address of the member.</param>
        /// <param name="kontakt">The contact information of the member.</param>
        /// <param name="datumNastupu">The date of joining the fire brigade.</param>
        /// <param name="funkce">The role or function of the member in the fire brigade.</param>
        public void Insert(string jmeno, string prijmeni, DateTime datumNarozeni, string adresa, string kontakt,
            DateTime datumNastupu, string funkce)
        {
            try
            {
                using DatabaseConnection d = DatabaseConnection.Instance;
                using SqlCommand command =
                    new SqlCommand(
                        "INSERT INTO ClenoveSboru VALUES (@value1, @value2, @value3, @value4, @value5, @value6, @value7)",
                        d.Connection);
                command.Parameters.AddWithValue("@value1", jmeno);
                command.Parameters.AddWithValue("@value2", prijmeni);
                command.Parameters.AddWithValue("@value3", datumNarozeni);
                command.Parameters.AddWithValue("@value4", adresa);
                command.Parameters.AddWithValue("@value5", kontakt);
                command.Parameters.AddWithValue("@value6", datumNastupu);
                command.Parameters.AddWithValue("@value7", funkce);

                int rowsAffected = command.ExecuteNonQuery();
                Console.WriteLine($"Rows affected: {rowsAffected}");
            }
            catch (Exception ex)
            {
                LogError(ex);
                ShowError(
                    "An error occurred while inserting the record. Please try again or contact support for assistance.");
            }
        }

        /// <summary>
        /// Deletes a record from the "ClenoveSboru" table based on the provided ID.
        /// </summary>
        /// <param name="id">The ID of the record to be deleted.</param>
        public void Delete(int id)
        {
            try
            {
                using DatabaseConnection d = DatabaseConnection.Instance;
                using SqlCommand command =
                    new SqlCommand("DELETE FROM ClenoveSboru WHERE IDClena = @valeu1;", d.Connection);
                command.Parameters.AddWithValue("@value1", id);

                int rowsAffected = command.ExecuteNonQuery();
                Console.WriteLine($"Rows affected: {rowsAffected}");
            }
            catch (Exception ex)
            {
                LogError(ex);
                ShowError(
                    "An error occurred while deleting the record. Please try again or contact support for assistance.");
            }
        }

        /// <summary>
        /// Exports data from the "ClenoveSboru" table to a CSV file.
        /// </summary>
        public void Export()
        {
            string filePath =
                MainWindow.ConfigurationFileLoader.Configuration
                    .ClenoveSboruExportPath; // "C:\\Users\\Martin\\Desktop\\ClenoveSboruExport.csv"
            try
            {
                using DatabaseConnection d = DatabaseConnection.Instance;
                using SqlCommand command = new SqlCommand("SELECT * FROM ClenoveSboru", d.Connection);
                using SqlDataReader reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    using (StreamWriter writer = new StreamWriter(filePath))
                    {
                        // Write header (column names) to the file
                        for (int i = 0; i < reader.FieldCount; i++)
                        {
                            string columnName = reader.GetName(i);
                            writer.Write($"{columnName}");

                            if (i < reader.FieldCount - 1)
                            {
                                // Add a comma unless it's the last column
                                writer.Write(",");
                            }
                        }

                        writer.WriteLine(); // Move to the next line after writing the header

                        // Iterate through the rows and write data to the file
                        while (reader.Read())
                        {
                            for (int i = 0; i < reader.FieldCount; i++)
                            {
                                // Access columns by index or column name
                                object columnValue = reader.GetValue(i);

                                writer.Write($"{columnValue}");

                                if (i < reader.FieldCount - 1)
                                {
                                    // Add a comma unless it's the last column
                                    writer.Write(",");
                                }
                            }

                            writer.WriteLine(); // Move to the next line after writing the row
                        }
                    }

                    MessageBox.Show($"Exported data to {filePath}", "Table Exported");
                }
                else
                {
                    MessageBox.Show("No rows found", "Error");
                }
            }
            catch (Exception ex)
            {
                LogError(ex);
                ShowError(
                    "An error occurred while exporting data. Please try again or contact support for assistance.");
            }
        }

        /// <summary>
        /// Imports data from a CSV file into the "ClenoveSboru" table.
        /// </summary>
        /// <param name="filePath">The path of the CSV file to be imported.</param>
        public void Import(string filePath)
        {
            try
            {
                using DatabaseConnection d = DatabaseConnection.Instance;
                using TextFieldParser parser = new TextFieldParser(filePath);
                parser.Delimiters = new[] { "," };
                string[] columns = parser.ReadFields();
                string insertQuery = $"INSERT INTO ClenoveSboru ({string.Join(", ", columns)}) VALUES ";
                StringBuilder sb = new StringBuilder(insertQuery);
                for (int i = 0; i < columns.Length; i++)
                {
                    sb.Append("@param").Append(i);
                    if (i < columns.Length - 1)
                    {
                        sb.Append(", ");
                    }
                }

                string preparedInsertQuery = sb.ToString();

                // Read and insert data from each row
                while (!parser.EndOfData)
                {
                    string[] fields = parser.ReadFields();

                    // Remove any empty elements caused by trailing commas
                    fields = fields.Where(field => !string.IsNullOrEmpty(field)).ToArray();

                    // Prepare the parameters for the INSERT query
                    using SqlCommand command = new SqlCommand(preparedInsertQuery, d.Connection);
                    for (int i = 0; i < fields.Length; i++)
                    {
                        command.Parameters.AddWithValue($"@param{i}", fields[i]);
                    }

                    // Execute the INSERT query
                    command.ExecuteNonQuery();
                }

                MessageBox.Show($"Imported data from {filePath} to the database", "Table Imported");
            }
            catch (Exception ex)
            {
                LogError(ex);
                ShowError(
                    "An error occurred while importing data. Please try again or contact support for assistance.");
            }
        }

        /// <summary>
        /// Logs an error message to a file.
        /// </summary>
        /// <param name="ex">The exception to log.</param>
        private void LogError(Exception ex)
        {
            try
            {
                using StreamWriter logWriter = new StreamWriter(LogFilePath, true);
                logWriter.WriteLine($"[{DateTime.Now}] Error: {ex.Message}");
                logWriter.WriteLine($"StackTrace: {ex.StackTrace}");
                logWriter.WriteLine();
            }
            catch (IOException)
            {
                // If logging to file fails, ignore the exception
            }
        }

        /// <summary>
        /// Displays an error message dialog box.
        /// </summary>
        /// <param name="message">The error message to display.</param>
        private void ShowError(string message)
        {
            MessageBox.Show(message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
        }

        /// <summary>
        /// Updates data in the "ClenoveSboru" table based on the provided ID.
        /// </summary>
        /// <param name="updateCommand"></param>
        public void Update(string updateCommand)
        {
            try
            {
                using DatabaseConnection d = DatabaseConnection.Instance;
                using SqlCommand command = new SqlCommand(updateCommand, d.Connection);
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                LogError(ex);
                Console.WriteLine(ex);
                ShowError(
                    "An error occurred while updating the record. Please try again.");
            }
        }

        /// <summary>
        /// Selects all data from "ClenoveSboru" table.
        /// </summary>
        /// <returns>Returns all table records.</returns>
        public DataTable Select()
        {
            using DatabaseConnection d = DatabaseConnection.Instance;
            string query = "SELECT * FROM ClenoveSboru";
            SqlCommand command = new SqlCommand(query, d.Connection);
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            DataTable dataTable = new DataTable();
            adapter.Fill(dataTable);
            return dataTable;
        }
    }
}