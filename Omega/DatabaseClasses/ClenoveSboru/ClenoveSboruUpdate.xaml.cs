﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace Omega.DatabaseClasses.ClenoveSboru
{
    /// <summary>
    /// Window for updating member information.
    /// </summary>
    public partial class ClenoveSboruUpdate : Window
    {
        private DatabaseClenoveSboruGateway _d;
        private string _funkce;

        /// <summary>
        /// Initializes a new instance of the ClenoveSboruUpdate class.
        /// </summary>
        public ClenoveSboruUpdate()
        {
            InitializeComponent();
            _d = new DatabaseClenoveSboruGateway();
            LoadDataGrid();
        }

        private void dropdown_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox comboBox = (ComboBox)sender;
            ComboBoxItem selectedItem = (ComboBoxItem)comboBox.SelectedItem;
            _funkce = selectedItem.Content.ToString() ?? throw new InvalidOperationException();
        }

        /// <summary>
        /// Event handler for the update button click.
        /// </summary>
        /// <param name="sender">The object that triggered the event.</param>
        /// <param name="e">The event arguments.</param>
        private void UpdateTableButton_Click(object sender, RoutedEventArgs e)
        {
            string update = "UPDATE ClenoveSboru SET ";

            string jmeno = TextBoxName.Text;
            if (jmeno != "" && NameUpdateBool.IsChecked == false)
            {
                update += "Jmeno = '" + jmeno + "', ";
            }

            string prijmeni = TextBoxSurname.Text;
            if (prijmeni != "" && SurnameUpdateBool.IsChecked == false)
            {
                update += "Prijmeni = '" + prijmeni + "', ";
            }

            DateTime datumNarozeni = default;
            if (DatePickerBirth.SelectedDate != null && BirthDateUpdateBool.IsChecked == false)
            {
                datumNarozeni = DatePickerBirth.SelectedDate ?? DateTime.MinValue;
                update += "DatumNarozeni = '" + datumNarozeni.ToString("yyyy-MM-dd") + "', ";
            }

            string adresa = TextBoxAdress.Text;
            if (adresa != "" && AdressUpdateBool.IsChecked == false)
            {
                update += "Adresa = '" + adresa + "', ";
            }

            string kontakt = TextBoxContact.Text;
            if (kontakt != "" && ContactUpdateBool.IsChecked == false)
            {
                update += "KontaktniInformace = '" + kontakt + "', ";
            }

            DateTime datumNastupu = default;
            if (DatePickerStartDate.SelectedDate != null && StartDateUpdateBool.IsChecked == false)
            {
                datumNastupu = DatePickerStartDate.SelectedDate ?? DateTime.MinValue;
                update += "DatumPripojeni = '" + datumNastupu.ToString("yyyy-MM-dd") + "', ";
            }

            if (_funkce != "" && FunctionUpdateBool.IsChecked == false)
            {
                update += "Funkce = '" + _funkce + "', ";
            }

            if (!string.IsNullOrEmpty(update) && IdBoxName.Text != "")
            {
                if (int.TryParse(IdBoxName.Text, out _))
                {
                    update = update.Remove(update.Length - 2);
                    update += " WHERE IDClena = " + IdBoxName.Text;
                    _d.Update(update);
                    Close();
                }
                else
                {
                    MessageBox.Show("ID is not correct", "Error");
                }
            }
            else
            {
                MessageBox.Show("Some of the values are not correct", "Error");
            }
        }
        
        private void LoadDataGrid()
        {
            datagridClenove.ItemsSource = _d.Select().DefaultView;
        }
    }
}