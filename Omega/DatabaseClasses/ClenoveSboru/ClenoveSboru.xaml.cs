﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace Omega.DatabaseClasses.ClenoveSboru
{
    /// <summary>
    /// Represents the window for managing members of the fire brigade.
    /// </summary>
    public partial class ClenoveSboru : Window
    {
        private DatabaseClenoveSboruGateway _d;
        private string _funkce;

        /// <summary>
        /// Initializes a new instance of the <see cref="ClenoveSboru"/> class.
        /// </summary>
        public ClenoveSboru()
        {
            InitializeComponent();

            _d = new DatabaseClenoveSboruGateway();
        }

        private void dropdown_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            // Handle the selected item change
            ComboBox comboBox = (ComboBox)sender;
            ComboBoxItem selectedItem = (ComboBoxItem)comboBox.SelectedItem;
            _funkce = selectedItem.Content.ToString() ?? throw new InvalidOperationException();
        }

        /// <summary>
        /// Handles the event when the "Insert Into Table" button is clicked.
        /// Inserts a new member into the database with the provided information.
        /// </summary>
        /// <param name="sender">The object that triggered the event.</param>
        /// <param name="e">The event arguments.</param>
        private void InsertIntoTableButton_Click(object sender, RoutedEventArgs e)
        {
            int numberofcorrectinputs = 0;

            string jmeno = TextBoxName.Text;
            if (jmeno != "")
            {
                numberofcorrectinputs++;
            }

            string prijmeni = TextBoxSurname.Text;
            if (prijmeni != "")
            {
                numberofcorrectinputs++;
            }

            DateTime datumNarozeni = default;
            if (DatePickerBirth.SelectedDate != null)
            {
                datumNarozeni = DatePickerBirth.SelectedDate ?? DateTime.MinValue;
                numberofcorrectinputs++;
            }

            string adresa = TextBoxAdress.Text;
            if (adresa != "")
            {
                numberofcorrectinputs++;
            }

            string kontakt = TextBoxContact.Text;
            if (kontakt != "")
            {
                numberofcorrectinputs++;
            }

            DateTime datumNastupu = default;
            if (DatePickerStartDate.SelectedDate != null)
            {
                datumNastupu = DatePickerStartDate.SelectedDate ?? DateTime.MinValue;
                numberofcorrectinputs++;
            }

            if (_funkce != "")
            {
                numberofcorrectinputs++;
            }

            if (numberofcorrectinputs == 7)
            {
                _d.Insert(jmeno, prijmeni, datumNarozeni, adresa, kontakt, datumNastupu, _funkce);
                Close();
            }
            else
            {
                MessageBox.Show("Some of the values are not correct", "Error");
            }
        }
    }
}