﻿using System;
using System.Windows;

namespace Omega.DatabaseClasses.VozidlaTechnika
{
    /// <summary>
    /// Represents a window for interacting with the "VozidlaTechnika" table in the database.
    /// </summary>
    public partial class VozidlaTechnika : Window
    {
        private DatabaseVozidlaTechnikaGateway _d;

        /// <summary>
        /// Initializes a new instance of the <see cref="VozidlaTechnika"/> class.
        /// </summary>
        public VozidlaTechnika()
        {
            InitializeComponent();
            _d = new DatabaseVozidlaTechnikaGateway();
        }

        /// <summary>
        /// Handles the click event of the "InsertIntoTableButton" button to insert a new record into the "VozidlaTechnika" table.
        /// </summary>
        private void InsertIntoTableButton_Click(object sender, RoutedEventArgs e)
        {
            int numberofcorrectinputs = 0;

            string vehicleType = TextBoxType.Text;
            if (vehicleType != "")
            {
                numberofcorrectinputs++;
            }

            string registrationNumber = TextBoxRegistrtion.Text;
            if (registrationNumber != "")
            {
                numberofcorrectinputs++;
            }

            DateTime purchaseDate = default;
            if (DatePickerPurchase.SelectedDate != null)
            {
                purchaseDate = DatePickerPurchase.SelectedDate ?? DateTime.MinValue;
                numberofcorrectinputs++;
            }

            int isDrivable = 0;
            bool tmp = chkIsActive.IsChecked == true;
            if (tmp)
            {
                isDrivable = 1;
            }

            DateTime lastMaintanceDate = default;
            if (DatePickerMaintance.SelectedDate != null)
            {
                lastMaintanceDate = DatePickerMaintance.SelectedDate ?? DateTime.MinValue;
                numberofcorrectinputs++;
            }

            if (numberofcorrectinputs == 4)
            {
                _d.Insert(vehicleType, registrationNumber, purchaseDate, isDrivable, lastMaintanceDate);
                Close();
            }
            else
            {
                MessageBox.Show("Some of the values are not correct", "Error");
            }
        }
    }
}