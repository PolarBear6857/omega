﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Windows;
using Microsoft.VisualBasic.FileIO;

namespace Omega.DatabaseClasses.VozidlaTechnika
{
    /// <summary>
    /// Represents a gateway for interacting with the "VozidlaTechnika" table in the database.
    /// </summary>
    public class DatabaseVozidlaTechnikaGateway
    {
        /// <summary>
        /// Inserts a new record into the "VozidlaTechnika" table.
        /// </summary>
        /// <param name="vehicleType">The type of the vehicle.</param>
        /// <param name="registrationNumber">The registration number of the vehicle.</param>
        /// <param name="purchaseDate">The date of purchase for the vehicle.</param>
        /// <param name="isDrivable">Indicates whether the vehicle is drivable (1 for true, 0 for false).</param>
        /// <param name="lastMaintenanceDate">The date of the last maintenance for the vehicle.</param>
        public void Insert(string vehicleType, string registrationNumber, DateTime purchaseDate, int isDrivable,
            DateTime lastMaintanceDate)
        {
            using DatabaseConnection d = DatabaseConnection.Instance;
            using SqlCommand command =
                new SqlCommand(
                    "INSERT INTO VozidlaTechnika VALUES (@value1, @value2, @value3, @value4, @value5)",
                    d.Connection);
            command.Parameters.AddWithValue("@value1", vehicleType);
            command.Parameters.AddWithValue("@value2", registrationNumber);
            command.Parameters.AddWithValue("@value3", purchaseDate);
            command.Parameters.AddWithValue("@value4", isDrivable);
            command.Parameters.AddWithValue("@value5", lastMaintanceDate);
            try
            {
                // Execute the query
                int rowsAffected = command.ExecuteNonQuery();
                Console.WriteLine($"Rows affected: {rowsAffected}");
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error: {ex.Message}");
            }
        }

        /// <summary>
        /// Deletes a record from the "VozidlaTechnika" table based on the specified ID.
        /// </summary>
        /// <param name="id">The ID of the record to be deleted.</param>
        public void Delete(int id)
        {
            using DatabaseConnection d = DatabaseConnection.Instance;
            using SqlCommand command = new SqlCommand("DELETE FROM VozidlaTechnika WHERE IDVozidla = @valeu1;",
                d.Connection);
            command.Parameters.AddWithValue("@value1", id);
            try
            {
                // Execute the query
                int rowsAffected = command.ExecuteNonQuery();
                Console.WriteLine($"Rows affected: {rowsAffected}");
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error: {ex.Message}");
            }
        }

        /// <summary>
        /// Exports the data from the "VozidlaTechnika" table to a CSV file.
        /// </summary>
        public void Export()
        {
            string filePath =
                MainWindow.ConfigurationFileLoader.Configuration
                    .VozidlaTechnikaExportPath;
            using DatabaseConnection d = DatabaseConnection.Instance;
            using SqlCommand command = new SqlCommand("SELECT * FROM VozidlaTechnika", d.Connection);
            try
            {
                using SqlDataReader reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    using (StreamWriter writer = new StreamWriter(filePath))
                    {
                        // Write header (column names) to the file
                        for (int i = 0; i < reader.FieldCount; i++)
                        {
                            string columnName = reader.GetName(i);
                            writer.Write($"{columnName}");

                            if (i < reader.FieldCount - 1)
                            {
                                // Add a comma unless it's the last column
                                writer.Write(",");
                            }
                        }

                        writer.WriteLine(); // Move to the next line after writing the header

                        // Iterate through the rows and write data to the file
                        while (reader.Read())
                        {
                            for (int i = 0; i < reader.FieldCount; i++)
                            {
                                // Access columns by index or column name
                                object columnValue = reader.GetValue(i);

                                writer.Write($"{columnValue}");

                                if (i < reader.FieldCount - 1)
                                {
                                    // Add a comma unless it's the last column
                                    writer.Write(",");
                                }
                            }

                            writer.WriteLine(); // Move to the next line after writing the row
                        }
                    }

                    MessageBox.Show($"Exported data to {filePath}", "Table Exported");
                }
                else
                {
                    MessageBox.Show("No rows found", "Error");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error: {ex.Message}");
            }
        }

        /// <summary>
        /// Imports data from a CSV file into the "VozidlaTechnika" table.
        /// </summary>
        /// <param name="filePath">The path to the CSV file.</param>
        public void Import(string filePath)
        {
            using DatabaseConnection d = DatabaseConnection.Instance;
            try
            {
                // Create a TextFieldParser to read the CSV file
                using TextFieldParser parser = new TextFieldParser(filePath);
                parser.Delimiters = new string[] { "," };

                // Read the header (column names)
                string[] columns = parser.ReadFields();

                // Assume the first row contains column names; adjust if necessary
                // This example assumes that the CSV file has the same structure as the table
                // You may need to map CSV columns to database columns if they are different
                string insertQuery = $"INSERT INTO VozidlaTechnika ({string.Join(", ", columns)}) VALUES ";

                // Read and insert data from each row
                while (!parser.EndOfData)
                {
                    string[] fields = parser.ReadFields();

                    // Remove any empty elements caused by trailing commas
                    fields = fields.Where(field => !string.IsNullOrEmpty(field)).ToArray();

                    // Build the VALUES part of the SQL INSERT query
                    string values = $"('{string.Join("', '", fields)}')";

                    Console.WriteLine(insertQuery + values);

                    // Execute the INSERT query
                    using SqlCommand command = new SqlCommand(insertQuery + values, d.Connection);
                    command.ExecuteNonQuery();
                }

                MessageBox.Show($"Imported data from {filePath} to the database", "Table Imported");
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error: {ex.Message}");
            }
        }

        /// <summary>
        /// Updates data in the "VozidlaTechnika" table based on the provided ID.
        /// </summary>
        /// <param name="updateCommand"></param>
        public void Update(string updateCommand)
        {
            using DatabaseConnection d = DatabaseConnection.Instance;
            try
            {
                using SqlCommand command = new SqlCommand(updateCommand, d.Connection);
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error: {ex.Message}");
            }
        }

        /// <summary>
        /// Selects all data from "VozidlaTechnika" table.
        /// </summary>
        /// <returns>Returns all table records.</returns>
        public DataTable Select()
        {
            using DatabaseConnection d = DatabaseConnection.Instance;
            string query = "SELECT * FROM VozidlaTechnika";
            SqlCommand command = new SqlCommand(query, d.Connection);
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            DataTable dataTable = new DataTable();
            adapter.Fill(dataTable);
            return dataTable;
        }
    }
}