﻿using System;
using System.Windows;

namespace Omega.DatabaseClasses.VozidlaTechnika
{
    /// <summary>
    /// Window for updating vehicle technical information.
    /// </summary>
    public partial class VozidlaTechnikaUpdate : Window
    {
        private DatabaseVozidlaTechnikaGateway _d;

        /// <summary>
        /// Initializes a new instance of the VozidlaTechnikaUpdate class.
        /// </summary>
        public VozidlaTechnikaUpdate()
        {
            InitializeComponent();
            _d = new DatabaseVozidlaTechnikaGateway();
            LoadDataGrid();
        }

        /// <summary>
        /// Event handler for the update button click.
        /// </summary>
        /// <param name="sender">The object that triggered the event.</param>
        /// <param name="e">The event arguments.</param>
        private void UpdateTableButton_Click(object sender, RoutedEventArgs e)
        {
            string update = "UPDATE VozidlaTechnika SET ";

            if (TextBoxType.Text != "" && TypeUpdateBool.IsChecked == false)
            {
                update += "TypVozidla = '" + TextBoxType.Text + "', ";
            }

            if (TextBoxRegistrtion.Text != "" && RegistrationUpdateBool.IsChecked == false)
            {
                update += "RegCislo = '" + TextBoxRegistrtion.Text + "', ";
            }

            DateTime purchaseDate = default;
            if (DatePickerPurchase.SelectedDate != null && PurchaseUpdateBool.IsChecked == false)
            {
                purchaseDate = DatePickerPurchase.SelectedDate ?? DateTime.MinValue;
                update += "DatumZakoupeni = '" + purchaseDate.ToString("yyyy-MM-dd") + "', ";
            }

            if (chkIsActive.IsChecked == true && ActiveUpdateBool.IsChecked == false)
            {
                update += "TechnickyStav = '1', ";
            }

            if (chkIsActive.IsChecked == false && ActiveUpdateBool.IsChecked == false)
            {
                update += "TechnickyStav = '0', ";
            }

            DateTime lastMaintanceDate = default;
            if (DatePickerMaintance.SelectedDate != null && MaintanceUpdateBool.IsChecked == false)
            {
                lastMaintanceDate = DatePickerMaintance.SelectedDate ?? DateTime.MinValue;
                update += "PravidelnaUdrzba = '" + lastMaintanceDate.ToString("yyyy-MM-dd") + "', ";
            }

            if (!string.IsNullOrEmpty(update) && TextBoxID.Text != "")
            {
                if (int.TryParse(TextBoxID.Text, out _))
                {
                    update = update.Remove(update.Length - 2);
                    update += " WHERE IDVozidla = " + TextBoxID.Text;
                    _d.Update(update);
                    Close();
                }
                else
                {
                    MessageBox.Show("ID is not correct", "Error");
                }
            }
            else
            {
                MessageBox.Show("Some of the values are not correct", "Error");
            }
        }
        
        private void LoadDataGrid()
        {
            datagridVozidloTechnika.ItemsSource = _d.Select().DefaultView;
        }
    }
}