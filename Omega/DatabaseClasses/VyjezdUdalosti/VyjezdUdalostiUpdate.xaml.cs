﻿using System.Windows;
using Omega.DatabaseClasses.VozidlaTechnika;

namespace Omega.DatabaseClasses.VyjezdUdalosti
{
    /// <summary>
    /// Window for updating VyjezdUdalosti information.
    /// </summary>
    public partial class VyjezdUdalostiUpdate : Window
    {
        private DatabaseVyjezdUdalostiGateway _d;

        /// <summary>
        /// Initializes a new instance of the VyjezdUdalostiUpdate class.
        /// </summary>
        public VyjezdUdalostiUpdate()
        {
            InitializeComponent();
            _d = new DatabaseVyjezdUdalostiGateway();
            LoadDataGrid();
        }

        /// <summary>
        /// Event handler for the insert button click.
        /// </summary>
        /// <param name="sender">The object that triggered the event.</param>
        /// <param name="e">The event arguments.</param>
        private void UpdateTableButton_Click(object sender, RoutedEventArgs e)
        {
            string update = "";

            string popisUdalosti = TextBoxDescription.Text;
            if (popisUdalosti != "" && DescUpdateBool.IsChecked == false)
            {
                update += "UPDATE VozidlaTechnika SET PopisUdalosti = '" + popisUdalosti + "', ";
            }

            if (!string.IsNullOrEmpty(update) && TextBoxID.Text != "")
            {
                if (int.TryParse(TextBoxID.Text, out _))
                {
                    update = update.Remove(update.Length - 2);
                    update += " WHERE IDUdalosti = " + TextBoxID.Text;
                    _d.Update(update);
                    Close();
                }
                else
                {
                    MessageBox.Show("ID is not correct", "Error");
                }
            }
            else
            {
                MessageBox.Show("Some of the values are not correct", "Error");
            }
        }
        
        private void LoadDataGrid()
        {
            datagridVyjezdUdalosti.ItemsSource = _d.Select().DefaultView;
        }
    }
}