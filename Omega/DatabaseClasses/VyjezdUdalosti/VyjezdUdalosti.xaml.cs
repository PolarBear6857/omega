﻿using System.Windows;

namespace Omega.DatabaseClasses.VyjezdUdalosti
{
    /// <summary>
    /// Represents a gateway for interacting with the "VyjezdUdalosti" table in the database.
    /// </summary>
    public partial class VyjezdUdalosti : Window
    {
        private DatabaseVyjezdUdalostiGateway _d;

        /// <summary>
        /// Initializes a new instance of the <see cref="VyjezdUdalosti"/> class.
        /// </summary>
        public VyjezdUdalosti()
        {
            InitializeComponent();
            _d = new DatabaseVyjezdUdalostiGateway();
        }

        /// <summary>
        /// Event handler for the "InsertIntoTableButton" click event.
        /// Inserts a new record into the "VyjezdUdalosti" table.
        /// </summary>
        /// <param name="sender">The event sender.</param>
        /// <param name="e">The event arguments.</param>
        private void InsertIntoTableButton_Click(object sender, RoutedEventArgs e)
        {
            string popisUdalosti = TextBoxDescription.Text;

            if (popisUdalosti != "")
            {
                _d.Insert(popisUdalosti);
                Close();
            }
            else
            {
                MessageBox.Show("Some of the values are not correct", "Error");
            }
        }
    }
}