var namespace_omega_1_1_database_classes =
[
    [ "ClenoveSboru", "namespace_omega_1_1_database_classes_1_1_clenove_sboru.html", "namespace_omega_1_1_database_classes_1_1_clenove_sboru" ],
    [ "ClenoveSmeny", "namespace_omega_1_1_database_classes_1_1_clenove_smeny.html", "namespace_omega_1_1_database_classes_1_1_clenove_smeny" ],
    [ "Log", "namespace_omega_1_1_database_classes_1_1_log.html", "namespace_omega_1_1_database_classes_1_1_log" ],
    [ "Smeny", "namespace_omega_1_1_database_classes_1_1_smeny.html", "namespace_omega_1_1_database_classes_1_1_smeny" ],
    [ "VozidlaTechnika", "namespace_omega_1_1_database_classes_1_1_vozidla_technika.html", "namespace_omega_1_1_database_classes_1_1_vozidla_technika" ],
    [ "Vyjezd", "namespace_omega_1_1_database_classes_1_1_vyjezd.html", "namespace_omega_1_1_database_classes_1_1_vyjezd" ],
    [ "VyjezdUdalosti", "namespace_omega_1_1_database_classes_1_1_vyjezd_udalosti.html", "namespace_omega_1_1_database_classes_1_1_vyjezd_udalosti" ],
    [ "DatabaseConnection", "class_omega_1_1_database_classes_1_1_database_connection.html", "class_omega_1_1_database_classes_1_1_database_connection" ]
];