var annotated_dup =
[
    [ "Omega", "namespace_omega.html", [
      [ "DatabaseClasses", "namespace_omega_1_1_database_classes.html", [
        [ "ClenoveSboru", "namespace_omega_1_1_database_classes_1_1_clenove_sboru.html", [
          [ "ClenoveSboru", "class_omega_1_1_database_classes_1_1_clenove_sboru_1_1_clenove_sboru.html", "class_omega_1_1_database_classes_1_1_clenove_sboru_1_1_clenove_sboru" ],
          [ "ClenoveSboruUpdate", "class_omega_1_1_database_classes_1_1_clenove_sboru_1_1_clenove_sboru_update.html", "class_omega_1_1_database_classes_1_1_clenove_sboru_1_1_clenove_sboru_update" ],
          [ "DatabaseClenoveSboruGateway", "class_omega_1_1_database_classes_1_1_clenove_sboru_1_1_database_clenove_sboru_gateway.html", "class_omega_1_1_database_classes_1_1_clenove_sboru_1_1_database_clenove_sboru_gateway" ]
        ] ],
        [ "ClenoveSmeny", "namespace_omega_1_1_database_classes_1_1_clenove_smeny.html", [
          [ "ClenoveSmeny", "class_omega_1_1_database_classes_1_1_clenove_smeny_1_1_clenove_smeny.html", "class_omega_1_1_database_classes_1_1_clenove_smeny_1_1_clenove_smeny" ],
          [ "ClenoveSmenyUpdate", "class_omega_1_1_database_classes_1_1_clenove_smeny_1_1_clenove_smeny_update.html", "class_omega_1_1_database_classes_1_1_clenove_smeny_1_1_clenove_smeny_update" ],
          [ "DatabaseClenoveSmenyGateway", "class_omega_1_1_database_classes_1_1_clenove_smeny_1_1_database_clenove_smeny_gateway.html", "class_omega_1_1_database_classes_1_1_clenove_smeny_1_1_database_clenove_smeny_gateway" ]
        ] ],
        [ "Log", "namespace_omega_1_1_database_classes_1_1_log.html", [
          [ "DatabaseLogGateway", "class_omega_1_1_database_classes_1_1_log_1_1_database_log_gateway.html", "class_omega_1_1_database_classes_1_1_log_1_1_database_log_gateway" ]
        ] ],
        [ "Smeny", "namespace_omega_1_1_database_classes_1_1_smeny.html", [
          [ "DatabaseSmenyGateway", "class_omega_1_1_database_classes_1_1_smeny_1_1_database_smeny_gateway.html", "class_omega_1_1_database_classes_1_1_smeny_1_1_database_smeny_gateway" ],
          [ "Smeny", "class_omega_1_1_database_classes_1_1_smeny_1_1_smeny.html", "class_omega_1_1_database_classes_1_1_smeny_1_1_smeny" ],
          [ "SmenyUpdate", "class_omega_1_1_database_classes_1_1_smeny_1_1_smeny_update.html", "class_omega_1_1_database_classes_1_1_smeny_1_1_smeny_update" ]
        ] ],
        [ "VozidlaTechnika", "namespace_omega_1_1_database_classes_1_1_vozidla_technika.html", [
          [ "DatabaseVozidlaTechnikaGateway", "class_omega_1_1_database_classes_1_1_vozidla_technika_1_1_database_vozidla_technika_gateway.html", "class_omega_1_1_database_classes_1_1_vozidla_technika_1_1_database_vozidla_technika_gateway" ],
          [ "VozidlaTechnika", "class_omega_1_1_database_classes_1_1_vozidla_technika_1_1_vozidla_technika.html", "class_omega_1_1_database_classes_1_1_vozidla_technika_1_1_vozidla_technika" ],
          [ "VozidlaTechnikaUpdate", "class_omega_1_1_database_classes_1_1_vozidla_technika_1_1_vozidla_technika_update.html", "class_omega_1_1_database_classes_1_1_vozidla_technika_1_1_vozidla_technika_update" ]
        ] ],
        [ "Vyjezd", "namespace_omega_1_1_database_classes_1_1_vyjezd.html", [
          [ "DatabaseVyjezdGateway", "class_omega_1_1_database_classes_1_1_vyjezd_1_1_database_vyjezd_gateway.html", "class_omega_1_1_database_classes_1_1_vyjezd_1_1_database_vyjezd_gateway" ],
          [ "Vyjezd", "class_omega_1_1_database_classes_1_1_vyjezd_1_1_vyjezd.html", "class_omega_1_1_database_classes_1_1_vyjezd_1_1_vyjezd" ],
          [ "VyjezdUpdate", "class_omega_1_1_database_classes_1_1_vyjezd_1_1_vyjezd_update.html", "class_omega_1_1_database_classes_1_1_vyjezd_1_1_vyjezd_update" ]
        ] ],
        [ "VyjezdUdalosti", "namespace_omega_1_1_database_classes_1_1_vyjezd_udalosti.html", [
          [ "DatabaseVyjezdUdalostiGateway", "class_omega_1_1_database_classes_1_1_vyjezd_udalosti_1_1_database_vyjezd_udalosti_gateway.html", "class_omega_1_1_database_classes_1_1_vyjezd_udalosti_1_1_database_vyjezd_udalosti_gateway" ],
          [ "VyjezdUdalosti", "class_omega_1_1_database_classes_1_1_vyjezd_udalosti_1_1_vyjezd_udalosti.html", "class_omega_1_1_database_classes_1_1_vyjezd_udalosti_1_1_vyjezd_udalosti" ],
          [ "VyjezdUdalostiUpdate", "class_omega_1_1_database_classes_1_1_vyjezd_udalosti_1_1_vyjezd_udalosti_update.html", "class_omega_1_1_database_classes_1_1_vyjezd_udalosti_1_1_vyjezd_udalosti_update" ]
        ] ],
        [ "DatabaseConnection", "class_omega_1_1_database_classes_1_1_database_connection.html", "class_omega_1_1_database_classes_1_1_database_connection" ]
      ] ],
      [ "lib", "namespace_omega_1_1lib.html", [
        [ "Configuration", "class_omega_1_1lib_1_1_configuration.html", "class_omega_1_1lib_1_1_configuration" ],
        [ "ConfigurationFileLoader", "class_omega_1_1lib_1_1_configuration_file_loader.html", "class_omega_1_1lib_1_1_configuration_file_loader" ]
      ] ],
      [ "OperationHandlers", "namespace_omega_1_1_operation_handlers.html", [
        [ "DeleteHandler", "class_omega_1_1_operation_handlers_1_1_delete_handler.html", "class_omega_1_1_operation_handlers_1_1_delete_handler" ],
        [ "ExportHandler", "class_omega_1_1_operation_handlers_1_1_export_handler.html", "class_omega_1_1_operation_handlers_1_1_export_handler" ],
        [ "ImportHandler", "class_omega_1_1_operation_handlers_1_1_import_handler.html", "class_omega_1_1_operation_handlers_1_1_import_handler" ],
        [ "InsertHandler", "class_omega_1_1_operation_handlers_1_1_insert_handler.html", "class_omega_1_1_operation_handlers_1_1_insert_handler" ],
        [ "UpdateHandler", "class_omega_1_1_operation_handlers_1_1_update_handler.html", "class_omega_1_1_operation_handlers_1_1_update_handler" ]
      ] ],
      [ "App", "class_omega_1_1_app.html", "class_omega_1_1_app" ],
      [ "MainWindow", "class_omega_1_1_main_window.html", "class_omega_1_1_main_window" ]
    ] ]
];