var namespace_omega_1_1_operation_handlers =
[
    [ "DeleteHandler", "class_omega_1_1_operation_handlers_1_1_delete_handler.html", "class_omega_1_1_operation_handlers_1_1_delete_handler" ],
    [ "ExportHandler", "class_omega_1_1_operation_handlers_1_1_export_handler.html", "class_omega_1_1_operation_handlers_1_1_export_handler" ],
    [ "ImportHandler", "class_omega_1_1_operation_handlers_1_1_import_handler.html", "class_omega_1_1_operation_handlers_1_1_import_handler" ],
    [ "InsertHandler", "class_omega_1_1_operation_handlers_1_1_insert_handler.html", "class_omega_1_1_operation_handlers_1_1_insert_handler" ],
    [ "UpdateHandler", "class_omega_1_1_operation_handlers_1_1_update_handler.html", "class_omega_1_1_operation_handlers_1_1_update_handler" ]
];