var searchData=
[
  ['databaseclenovesborugateway_0',['DatabaseClenoveSboruGateway',['../class_omega_1_1_database_classes_1_1_clenove_sboru_1_1_database_clenove_sboru_gateway.html',1,'Omega::DatabaseClasses::ClenoveSboru']]],
  ['databaseclenovesborugateway_2ecs_1',['DatabaseClenoveSboruGateway.cs',['../_database_clenove_sboru_gateway_8cs.html',1,'']]],
  ['databaseclenovesmenygateway_2',['DatabaseClenoveSmenyGateway',['../class_omega_1_1_database_classes_1_1_clenove_smeny_1_1_database_clenove_smeny_gateway.html',1,'Omega::DatabaseClasses::ClenoveSmeny']]],
  ['databaseclenovesmenygateway_2ecs_3',['DatabaseClenoveSmenyGateway.cs',['../_database_clenove_smeny_gateway_8cs.html',1,'']]],
  ['databaseconnection_4',['DatabaseConnection',['../class_omega_1_1_database_classes_1_1_database_connection.html',1,'Omega::DatabaseClasses']]],
  ['databaseconnection_2ecs_5',['DatabaseConnection.cs',['../_database_connection_8cs.html',1,'']]],
  ['databaseconnectionstring_6',['DatabaseConnectionString',['../class_omega_1_1lib_1_1_configuration.html#a96f0b71b5c082560dbe9d2a0ca26d461',1,'Omega::lib::Configuration']]],
  ['databaseloggateway_7',['DatabaseLogGateway',['../class_omega_1_1_database_classes_1_1_log_1_1_database_log_gateway.html',1,'Omega::DatabaseClasses::Log']]],
  ['databaseloggateway_2ecs_8',['DatabaseLogGateway.cs',['../_database_log_gateway_8cs.html',1,'']]],
  ['databasesmenygateway_9',['DatabaseSmenyGateway',['../class_omega_1_1_database_classes_1_1_smeny_1_1_database_smeny_gateway.html',1,'Omega::DatabaseClasses::Smeny']]],
  ['databasesmenygateway_2ecs_10',['DatabaseSmenyGateway.cs',['../_database_smeny_gateway_8cs.html',1,'']]],
  ['databasevozidlatechnikagateway_11',['DatabaseVozidlaTechnikaGateway',['../class_omega_1_1_database_classes_1_1_vozidla_technika_1_1_database_vozidla_technika_gateway.html',1,'Omega::DatabaseClasses::VozidlaTechnika']]],
  ['databasevozidlatechnikagateway_2ecs_12',['DatabaseVozidlaTechnikaGateway.cs',['../_database_vozidla_technika_gateway_8cs.html',1,'']]],
  ['databasevyjezdgateway_13',['DatabaseVyjezdGateway',['../class_omega_1_1_database_classes_1_1_vyjezd_1_1_database_vyjezd_gateway.html',1,'Omega::DatabaseClasses::Vyjezd']]],
  ['databasevyjezdgateway_2ecs_14',['DatabaseVyjezdGateway.cs',['../_database_vyjezd_gateway_8cs.html',1,'']]],
  ['databasevyjezdudalostigateway_15',['DatabaseVyjezdUdalostiGateway',['../class_omega_1_1_database_classes_1_1_vyjezd_udalosti_1_1_database_vyjezd_udalosti_gateway.html',1,'Omega::DatabaseClasses::VyjezdUdalosti']]],
  ['databasevyjezdudalostigateway_2ecs_16',['DatabaseVyjezdUdalostiGateway.cs',['../_database_vyjezd_udalosti_gateway_8cs.html',1,'']]],
  ['delete_17',['Delete',['../class_omega_1_1_database_classes_1_1_clenove_sboru_1_1_database_clenove_sboru_gateway.html#ae4963769dfd4a27502820112e12c6066',1,'Omega.DatabaseClasses.ClenoveSboru.DatabaseClenoveSboruGateway.Delete()'],['../class_omega_1_1_database_classes_1_1_clenove_smeny_1_1_database_clenove_smeny_gateway.html#afc4108e317e964d240b75364724384d1',1,'Omega.DatabaseClasses.ClenoveSmeny.DatabaseClenoveSmenyGateway.Delete()'],['../class_omega_1_1_database_classes_1_1_smeny_1_1_database_smeny_gateway.html#a13bb846052c15d766c35439bf7337aaf',1,'Omega.DatabaseClasses.Smeny.DatabaseSmenyGateway.Delete()'],['../class_omega_1_1_database_classes_1_1_vozidla_technika_1_1_database_vozidla_technika_gateway.html#a38ebb30f800d2e9f13fe44965f5ae395',1,'Omega.DatabaseClasses.VozidlaTechnika.DatabaseVozidlaTechnikaGateway.Delete()'],['../class_omega_1_1_database_classes_1_1_vyjezd_1_1_database_vyjezd_gateway.html#a48afa1621adf169105403a23be9e77e9',1,'Omega.DatabaseClasses.Vyjezd.DatabaseVyjezdGateway.Delete()'],['../class_omega_1_1_database_classes_1_1_vyjezd_udalosti_1_1_database_vyjezd_udalosti_gateway.html#aae37f2d811440624d05628c47e04829f',1,'Omega.DatabaseClasses.VyjezdUdalosti.DatabaseVyjezdUdalostiGateway.Delete()']]],
  ['deletehandler_18',['DeleteHandler',['../class_omega_1_1_operation_handlers_1_1_delete_handler.html',1,'Omega.OperationHandlers.DeleteHandler'],['../class_omega_1_1_operation_handlers_1_1_delete_handler.html#a3d3b88d4b8f55ab2d3ad44a41905e75a',1,'Omega.OperationHandlers.DeleteHandler.DeleteHandler()']]],
  ['deletehandler_2ecs_19',['DeleteHandler.cs',['../_delete_handler_8cs.html',1,'']]],
  ['dispose_20',['Dispose',['../class_omega_1_1_database_classes_1_1_database_connection.html#a947c39043256ea11a6c3ea4c55aa62f9',1,'Omega.DatabaseClasses.DatabaseConnection.Dispose()'],['../class_omega_1_1_database_classes_1_1_database_connection.html#ac24abfa0a11878b6d1e52c3f256d85ff',1,'Omega.DatabaseClasses.DatabaseConnection.Dispose(bool disposing)']]]
];
