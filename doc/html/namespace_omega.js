var namespace_omega =
[
    [ "DatabaseClasses", "namespace_omega_1_1_database_classes.html", "namespace_omega_1_1_database_classes" ],
    [ "lib", "namespace_omega_1_1lib.html", "namespace_omega_1_1lib" ],
    [ "OperationHandlers", "namespace_omega_1_1_operation_handlers.html", "namespace_omega_1_1_operation_handlers" ],
    [ "App", "class_omega_1_1_app.html", "class_omega_1_1_app" ],
    [ "MainWindow", "class_omega_1_1_main_window.html", "class_omega_1_1_main_window" ]
];