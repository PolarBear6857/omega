AUTOR: MARTIN KRÚPA

TŘÍDA: C4a

PROJEKT: OMEGA

- ------------------------------------------------------
# APLIKACE
1) PŘÍPRAVA DATABÁZE
    - spusťte Microsoft SQL server management studio
    - vytvořte databázi s názvem HZS_CR
    - v této databázi spusťte přiložený HZS.sql soubor
    - databáze by nyní měla obsahovat tabulky:
        - ClenoveSboru
        - ClenoveSmeny
        - LogTabulka
        - Smeny
        - VozidlaTechnika
        - Vyjezd
        - VyjezdUdalosti
2) SPUŠTĚNÍ APLIKACE
    - upravte přiložený config.json soubor
        - nastavte database connection string -> slouží pro připojení k databázi
        - nastavte cesty pro export tabulek -> adresář, kam se uloží výsledné csv soubory
    - spusťte přiložený Omega.exe soubor
    - vyberte konfigurační json soubor, který jste předtím upravili
3) POUŽÍVÁNÍ APLIKACE
    - tlačítka:
      1) Insert
            - Výběr tabulky (case insensitive):
                - clenovesboru
                - clenovesmeny
                - smeny
                - vozidlatechnika
                - vyjezd
                - vyjezdudalosti
            - Zadání dat
      2) Delete
            - Výběr tabulky (case insensitive):
                - clenovesboru
                - clenovesmeny
                - smeny
                - vozidlatechnika
                - vyjezd
                - vyjezdudalosti
            - zadání ID záznamu ke smazání
        3)  Update
            - Výběr tabulky (case insensitive):
                - clenovesboru
                - clenovesmeny
                - smeny
                - vozidlatechnika
                - vyjezd
                - vyjezdudalosti
            - Zadání dat
      4) Export
            - Výběr tabulky (case insensitive):
                - clenovesboru
                - clenovesmeny
                - log
                - smeny
                - vozidlatechnika
                - vyjezd
                - vyjezdudalosti
            - Na obrazovku se vypíše, kam se soubor vyexportoval
      5) Import
            - Výběr tabulky (case insensitive):
                - clenovesboru
                - clenovesmeny
                - smeny
                - vozidlatechnika
                - vyjezd
                - vyjezdudalosti
            - Vybrání CSV souboru
      6) Config
            - Vybere konfigurační json soubor

--------------------------------------------------------------------------------------------------------
# DOKUMENTACE

Dokumentaci lze nalézt ve složce \doc a otevře se spuštěním souboru index.html

--------------------------------------------------------------------------------------------------------
