-- Autor: Martin Kr�pa
-- T��da: C4a
-- Projekt: Alfa3

INSERT INTO ClenoveSboru (Jmeno, Prijmeni, DatumNarozeni, Adresa, KontaktniInformace, DatumPripojeni, Funkce) 
VALUES 
    ('John', 'Doe', '1990-05-15', '123 Main St', 'john.doe@email.com', '2022-01-01', 'Firefighter'),
    ('Jane', 'Smith', '1985-08-22', '456 Oak St', 'jane.smith@email.com', '2022-02-01', 'Rescue Specialist'),
    ('Bob', 'Johnson', '1998-03-10', '789 Maple St', 'bob.johnson@email.com', '2022-03-15', 'Paramedic'),
    ('Alice', 'Williams', '1992-11-27', '101 Pine St', 'alice.williams@email.com', '2022-04-20', 'Fire Chief'),
    ('Charlie', 'Miller', '1980-07-05', '202 Cedar St', 'charlie.miller@email.com', '2022-05-30', 'Driver Operator');

INSERT INTO VozidlaTechnika (TypVozidla, RegCislo, DatumZakoupeni, Pojizdne, PosledniPUdrzba) 
VALUES 
    ('Fire Truck', 'ABC123', '2020-01-10', 1, '2022-01-15'),
    ('Dire Truck with Ladder', 'XYZ789', '2018-05-20', 1, '2021-12-05'),
    ('Rescue Vehicle', 'DEF456', '2019-09-15', 0, '2022-02-20'),
    ('Command Vehicle', 'GHI789', '2021-03-08', 1, '2022-03-25'),
    ('Hazmat Unit', 'JKL012', '2017-11-12', 0, '2021-11-30');

INSERT INTO VyjezdUdalosti (PopisUdalosti) 
VALUES 
    ('Building Fire'),
    ('Water Rescue'),
    ('Chemical Spill'),
    ('Traffic Accident'),
    ('Medical Emergency');

INSERT INTO Smeny (DatumCasZacatku, DatumCasKonce, Popis) 
VALUES 
    ('2022-01-01 08:00:00', '2022-01-01 16:00:00', 'Day Shift'),
    ('2022-02-15 16:00:00', '2022-02-16 00:00:00', 'Evening Shift'),
    ('2022-03-10 00:00:00', '2022-03-10 08:00:00', 'Night Shift'),
    ('2022-04-05 08:00:00', '2022-04-05 16:00:00', 'Day Shift'),
    ('2022-05-20 16:00:00', '2022-05-21 00:00:00', 'Evening Shift');

INSERT INTO ClenoveSmeny (IDSmeny, IDClena) 
VALUES 
    (1, 1),
    (1, 2),
    (2, 3),
    (3, 4),
    (4, 5);

INSERT INTO Vyjezd (DatumCasUdalosti, MistoUdalosti, Udalost, VyuzitaTechnikaVybaveni, PritomnaSmena) 
VALUES 
    ('2022-01-05 10:30:00', 'Main Street', 10, 1, 1),
    ('2022-02-20 14:45:00', 'River Bank', 11, 2, 2),
    ('2022-03-15 08:00:00', 'Industrial Area', 12, 3, 3),
    ('2022-04-10 18:20:00', 'Highway', 12, 4, 4),
    ('2022-05-25 12:00:00', 'Residential Area', 14, 5, 5);
