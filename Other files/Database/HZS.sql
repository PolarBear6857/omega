CREATE TABLE ClenoveSboru (
    IDClena INT PRIMARY KEY identity(1,1) not null,
    Jmeno NVARCHAR(50) not null,
    Prijmeni NVARCHAR(50) not null,
    DatumNarozeni DATETIME not null,
    Adresa NVARCHAR(100) not null,
    KontaktniInformace NVARCHAR(50) not null,
    DatumPripojeni DATETIME not null,
    Funkce NVARCHAR(50) not null
);

CREATE TABLE VozidlaTechnika (
    IDVozidla INT PRIMARY KEY identity(1,1) not null,
    TypVozidla NVARCHAR(50) not null,
    RegCislo NVARCHAR(20) not null,
    DatumZakoupeni DATETIME not null,
    Pojizdne bit not null, -- O = ne, 1 = ano
    PosledniPUdrzba DATE not null
);

CREATE TABLE VyjezdUdalosti (
    IDUdalosti INT PRIMARY KEY identity(1,1) not null,
    PopisUdalosti NVARCHAR(MAX) not null,
);


CREATE TABLE Smeny (
    IDSmeny INT PRIMARY KEY identity(1,1) not null,
    DatumCasZacatku DATETIME not null,
    DatumCasKonce DATETIME not null,
    Popis NVARCHAR(255) not null
);

CREATE TABLE ClenoveSmeny (
    ID INT PRIMARY KEY identity(1,1) not null,
    IDSmeny INT not null,
    IDClena INT not null,
    FOREIGN KEY (IDSmeny) REFERENCES Smeny(IDSmeny),
    FOREIGN KEY (IDClena) REFERENCES ClenoveSboru(IDClena)
);

create table Vyjezd (
	IDVyjezd int primary key identity(1,1) not null,
	DatumCasUdalosti DATETIME,
    MistoUdalosti NVARCHAR(100) not null,
	Udalost int foreign key references VyjezdUdalosti(IDUdalosti),
	VyuzitaTechnikaVybaveni int foreign key references VozidlaTechnika(IDVozidla),
	PritomnaSmena int foreign key references Smeny(IDSmeny)
);

CREATE TABLE LogTabulka (
    IDlog INT PRIMARY KEY identity(1,1) not null,
    datum datetime not null,
	typ_udalosti nvarchar(50) not null,
	tabulka nvarchar(50) not null
);